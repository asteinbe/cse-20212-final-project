/* Implementation: the generic tile class used in map generation.
 *
 */

#include<SDL/SDL.h>
#include<SDL/SDL_image.h>
#include <iostream>
#include"Tile.h"


Tile::Tile(int tileX, int tileY, int tileType)
{
  box.x = tileX;  //location coordinates--note the lack of verification (fix?)
  box.y = tileY;

  box.w = 40;   //hardcoded width and heights of the tiles in the tilesheet
  box.h = 40;

  type = tileType;
}

void Tile::toBackground(SDL_Surface *fromMap, SDL_Surface *toBackground, SDL_Rect *cp, int x, int y)
{
      apply_surface(x, y, fromMap, toBackground, &cp[type]);
}

int Tile::get_type()
{
  return type;
}

SDL_Rect Tile::get_box()
{
  return box;
}

int Tile::get_X()
{
  return box.x;
}

int Tile::get_Y()
{
  return box.y;
}
