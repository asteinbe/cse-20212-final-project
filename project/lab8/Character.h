#include "Entities.h"
#include "Tile.h"
#include "Map.h"
#include "WorldObjects.h"
#include "Destructibles.h"
#include <cmath>

//character class is base class of all moving objects

extern const int screenWidth;
extern const int screenHeight;
extern const int levelWidth;
extern const int levelHeight;
extern const int tileSize;
extern SDL_Rect camera;
extern SDL_Event event;
extern SDL_Surface* screen;
extern std::vector<Entities> placedObjects;
extern void apply_surface( int x, int y, SDL_Surface* source, SDL_Surface* destination, SDL_Rect* clip);
extern bool check_collision( SDL_Rect A, SDL_Rect B );
extern Map TheMap;

#ifndef CHARACTER_H
#define CHARACTER_H

//The character
class Character
{
    public:
        //Initializes the variables
        Character(int,int,int,int,SDL_Surface*);
	bool checkAllCollisions(WorldObjects*,Map,SDL_Rect);
	//Moves the character
        void move(WorldObjects*,Map);
	void spawn(WorldObjects*, Map);
        //Shows the character
        void showCharacter();
	virtual void set_clips() = 0;
        int getX();
	int getY();
	SDL_Rect getCollideRect();
    protected:
        //Character location and size
	SDL_Rect characterDims;
        //Character's rate of movement
        float xVelocity;
        float yVelocity;
        //Display status
        int frame;
        int status;
        //Collision detection block
        SDL_Rect collide_block;
        //The areas of the sprite sheet
        SDL_Rect clipsUp[ 5 ];
        SDL_Rect clipsDown[ 5 ];
        SDL_Rect clipsRight[ 5 ];
        SDL_Rect clipsLeft[ 5 ];
        //direction status
        int CharacterDown;
        int CharacterUp;
        int CharacterLeft;
        int CharacterRight;
        //Character's SDL_Surface
        SDL_Surface* character;
	// number of animation sprites in each direction
	int upAnimations;
        int downAnimations;
        int leftAnimations;
        int rightAnimations;
	// if this is true, the character cannot move
	bool stopped;
};

Character::Character(int initX, int initY, int height, int width, SDL_Surface* self)
{
    //Initialize size & movement variables
    characterDims.x = initX;
    characterDims.y = initY;
    characterDims.w = width;
    characterDims.h = height;
    xVelocity = 0;
    yVelocity = 0;
    collide_block.h = characterDims.h;
    collide_block.w = characterDims.w;
    collide_block.x = characterDims.x;
    collide_block.y = characterDims.y;

    character = self;

    CharacterDown = 0;
    CharacterUp = 1;
    CharacterLeft = 2;
    CharacterRight = 3;

    //Initialize animation variables
    frame = 0;
    status = CharacterDown;

    stopped = false;
}

bool Character::checkAllCollisions(WorldObjects* WorldObjectsPtr, Map myMap, SDL_Rect checkRect)
{
    int i;
    int j;
    int currentType;
    int tileCount = (myMap.get_width())*(myMap.get_height());
    SDL_Rect tileDims;
    std::vector<Destructibles> myBreakables = WorldObjectsPtr->getBreakablesVec();
    for(i=0;i<placedObjects.size();i++) //iterates through all Entities objects in placedObjects
    {
        for(j=0;j<placedObjects[i].getEntities().size();j++) //for each of these objects, iterate through the contained vector of MySurface pointers
        {
	    if( check_collision( ( placedObjects[i].getEntities())[j]->getDims(), checkRect ) )
            {
                return true;
            }
        }
    }
    for(i=0;i<myBreakables.size();i++) //iterates through all Destructibles objects in myBreakables
    {
        for(j=0;j<myBreakables[i].getDestructibles().size();j++) //for each of these objects, iterate through the contained vector of MySurface pointers
        {
	    if( check_collision( ( myBreakables[i].getDestructibles())[j]->getDims(), checkRect ) )
            {
                return true;
            }
        }
    }
    for(i=0;i<tileCount;i++)
    {
        currentType = myMap.get_tile_type( i );
	tileDims = myMap.get_tile_rect( i );
	if( check_collision( tileDims, checkRect ) && currentType == 0 )
	{
	    return true;
	}
    }
    return false;
}

void Character::move(WorldObjects* WorldObjectsPtr, Map myMap)
{
    // if the character is stopped, don't move.
    if(stopped)
	return;

    // Set the collision block to the new location
    float tempDimsX = characterDims.x + xVelocity;
    collide_block.x = tempDimsX;
    
    //If the character movement is not off of the map and the character does not
    // collide with anything, make the move (this part only checks the x direction)
    if( ( tempDimsX >= 0 ) && ( tempDimsX + characterDims.w <= levelWidth ) && !this->checkAllCollisions(WorldObjectsPtr, myMap, collide_block ) )
    {
	characterDims.x = tempDimsX;
    }
    collide_block.x = characterDims.x;

    // Set the collision block to the new location
    float tempDimsY = characterDims.y + yVelocity;
    collide_block.y = tempDimsY + (characterDims.h - collide_block.h);
    
    //If the character movement is not off of the map and the character does not
    // collide with anything, make the move (this part only checks the y direction)
    if( ( tempDimsY >= 0 ) && ( characterDims.y + characterDims.h <= levelHeight ) && !this->checkAllCollisions(WorldObjectsPtr, myMap, collide_block ) )
    {
	characterDims.y = tempDimsY;
    }
    collide_block.y = characterDims.y + (characterDims.h - collide_block.h);
}

void Character::spawn(WorldObjects* WorldObjectsPtr, Map myMap)
{
    int tryX = rand() % myMap.get_width() * 40;
    int tryY = rand() % myMap.get_height() * 40;
    characterDims.x = tryX;
    collide_block.x = characterDims.x;
    characterDims.y = tryY;
    collide_block.y = characterDims.y + (characterDims.h - collide_block.h);
    while ( checkAllCollisions( WorldObjectsPtr, myMap, collide_block ) )
    {
	tryX = rand() % myMap.get_width() * 40;
    	tryX = rand() % myMap.get_height() * 40;
    	characterDims.x = tryX;
    	collide_block.x = characterDims.x;
    	characterDims.y = tryY;
    	collide_block.y = characterDims.y + (characterDims.h - collide_block.h);
    }
}

// Draws the character to the screen
void Character::showCharacter()
{
    if( yVelocity < 0 && abs(yVelocity) >= abs(xVelocity))
    {
     	//Set the animation to up
        status = CharacterUp;

        //Move to the next frame in the animation
        frame++;
    }
    else if( yVelocity > 0 && abs(yVelocity) >= abs(xVelocity))
    {
     	//Set the animation to down
        status = CharacterDown;

        //Move to the next frame in the animation
        frame++;
    }
    else if( xVelocity < 0 )
    {
     	//Set the animation to left
        status = CharacterLeft;

        //Move to the next frame in the animation
        frame++;
    }
    else if( xVelocity > 0 )
    {
     	//Set the animation to right
        status = CharacterRight;

        //Move to the next frame in the animation
        frame++;
    }
    //If Character is standing
    else
    {
     	//Restart the animation
        frame = 0;
    }
    //Loop the animation
    if( status == CharacterRight )
    {
     	if ( frame >= rightAnimations )
        {
            frame = 0;
	}
    }
    else if( status == CharacterLeft )
    {
     	if ( frame >= leftAnimations )
        {
            frame = 0;
        }
    }
    else if( status == CharacterUp )
    {
     	if ( frame >= upAnimations )
        {
            frame = 0;
        }
    }
    else if( status == CharacterDown )
    {
     	if ( frame >= downAnimations )
        {
            frame = 0;
        }
    }

    //Show the character
    if( status == CharacterDown )
    {
     	apply_surface( characterDims.x - camera.x, characterDims.y - camera.y, character, screen, &clipsDown[ frame ] );
    }
    else if( status == CharacterUp )
    {
     	apply_surface( characterDims.x - camera.x, characterDims.y - camera.y, character, screen, &clipsUp[ frame ] );
    }
    else if( status == CharacterLeft )
    {
     	apply_surface( characterDims.x - camera.x, characterDims.y - camera.y, character, screen, &clipsLeft[ frame ] );
    }
    else if( status == CharacterRight )
    {
     	apply_surface( characterDims.x - camera.x, characterDims.y - camera.y, character, screen, &clipsRight[ frame ] );
    }
}

int Character::getX()
{
  return  characterDims.x;
}

int Character::getY()
{
  return  characterDims.y;
}

// returns the collide block of the character
SDL_Rect Character::getCollideRect()
{
  return collide_block;
}

#endif
