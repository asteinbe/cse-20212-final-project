/* Implementation: the map class.
 *
 */

#include<iostream>
#include<SDL/SDL.h>
#include<SDL/SDL_image.h>
#include<vector>
#include<unistd.h>
#include"Map.h"
#include"Tile.h"

Map::Map(int width, int height)
{
  int i, j;

  set_width(width);   //set the vec of vec x dimension
  set_height(height);  //set the vec of vec y dimension
  
  generate_vector(); //first generate the 0-valued vec of vecs 

  //  first_pass();  //first pass generates land tiles on top of default water

  generate_land_mass();  //generates land block-wise 

  mapTiles = new Tile*[map_width * map_height]; //allocates array of pointers 
                                               //to tiles
  
  //  fill_in_water();  //This fills in landlocked water, not used right now

  place_sand_grass();    //changes land tiles to "pure" sand or grass
  refine_land_textures();  //smoothes out sand-to-grass transitions 
  refine_land_texture_corners(); //smoothes corners on transition bounds

  for(j = 0; j < (get_height()); ++j)
    {
      for(i = 0; i < (get_width()); ++i) //create tiles with coordinates & type
        {
          mapTiles[i+(j*get_width())] = new Tile(40*i, 40*j, map[i][j]);
	}
    }

  set_map_tile_clips();

  map_surface = load_image("test_tiles.png", 75, 99, 127); //load map tilesheet

}  

Map::~Map()
{/*
  SDL_FreeSurface(map_surface);

  for(int i = 0; i < (map_width * map_height); ++i)
    {
      delete [] mapTiles[i];
    }*/
}

int Map::get_width() const
{
  return map_width;
}


void Map::set_width(int width)
{
  if(width < 100)
    {
      map_width = 100;  //need at least 100 tiles or it won't be pretty
    }
  else if(width%10 != 0)
    {
      map_width = (width + (10 - (width % 10) ) );
    }
  else
    {
      map_width = width;
    }
}


int Map::get_height() const
{
  return map_height;
}


void Map::set_height(int height)
{
  if(height < 100)
    {
      map_height = 180;  //need at least 100 tiles or it won't be pretty
    }
  else if(height % 10 !=0)
    {
      map_height = (height + (10 - (height % 10) ) );
    }
  else
    {
      map_height = height;
    }
}


void Map::set_map_tile_clips()
{

  map_tile_clips[water].x = 0;
  map_tile_clips[water].y = 0;
  map_tile_clips[water].w = 40;
  map_tile_clips[water].h = 40;

  map_tile_clips[land].x = 40;
  map_tile_clips[land].y = 0;
  map_tile_clips[land].w = 40;
  map_tile_clips[land].h = 40;
  
  map_tile_clips[grass].x = 280;   //the pure grass tile on the sheet
  map_tile_clips[grass].y = 81;
  map_tile_clips[grass].w = 40;
  map_tile_clips[grass].h = 40;
 
  map_tile_clips[sand].x = 120;  //the pure sand tile on the sheet
  map_tile_clips[sand].y = 161;
  map_tile_clips[sand].w = 40;
  map_tile_clips[sand].h = 40;

  //All of the tiles below are directional transition tiles b/t sand & grass

  map_tile_clips[upper_left].x = 40;
  map_tile_clips[upper_left].y = 81;
  map_tile_clips[upper_left].w = 40;
  map_tile_clips[upper_left].h = 40;

  map_tile_clips[upper].x = 120;
  map_tile_clips[upper].y = 81;
  map_tile_clips[upper].w = 40;
  map_tile_clips[upper].h = 40;

  map_tile_clips[upper_right].x = 200;
  map_tile_clips[upper_right].y = 81;
  map_tile_clips[upper_right].w = 40;
  map_tile_clips[upper_right].h = 40;

  map_tile_clips[right].x = 200;
  map_tile_clips[right].y = 160;
  map_tile_clips[right].w = 40;
  map_tile_clips[right].h = 40;

  map_tile_clips[lower_right].x = 200;
  map_tile_clips[lower_right].y = 241;
  map_tile_clips[lower_right].w = 40;
  map_tile_clips[lower_right].h = 40;

  map_tile_clips[lower].x = 120;
  map_tile_clips[lower].y = 241;
  map_tile_clips[lower].w = 40;
  map_tile_clips[lower].h = 40;

  map_tile_clips[lower_left].x = 40;
  map_tile_clips[lower_left].y = 241;
  map_tile_clips[lower_left].w = 40;
  map_tile_clips[lower_left].h = 40;

  map_tile_clips[left].x = 40;
  map_tile_clips[left].y = 161;
  map_tile_clips[left].w = 40;
  map_tile_clips[left].h = 40;

  map_tile_clips[thick_up_left].x = 280;
  map_tile_clips[thick_up_left].y = 161;
  map_tile_clips[thick_up_left].w = 40;
  map_tile_clips[thick_up_left].h = 40;

  map_tile_clips[thick_down_left].x = 280;
  map_tile_clips[thick_down_left].y = 241;
  map_tile_clips[thick_down_left].w = 40;
  map_tile_clips[thick_down_left].h = 40;

  map_tile_clips[thick_up_right].x = 360;
  map_tile_clips[thick_up_right].y = 161;
  map_tile_clips[thick_up_right].w = 40;
  map_tile_clips[thick_up_right].h = 40;

  map_tile_clips[thick_down_right].x = 360;
  map_tile_clips[thick_down_right].y = 241;
  map_tile_clips[thick_down_right].w = 40;
  map_tile_clips[thick_down_right].h = 40;

}

SDL_Surface* Map::get_surface() const
{
  return map_surface;
}

					 
void Map::generate_vector()
{
  int i, j;
  for(i = 0; i < map_width; ++i)
    {
      std::vector<int> column;   //generate 0 values for each column
      for(j = 0; j < map_height; ++j)
	{
	  column.push_back(0);
	}
      map.push_back(column);  //push column onto row and get next column
    }
}

void Map::generate_land_mass()
{
  //first we seed the rand function, since this is a randomly generated map
  srand(SDL_GetTicks());

  //The idea here is that we will place block units of land, which are 
  //composed of individual tiles; the blocks make coherent generation
  //much easier--workable maps are the norm, not the exception
  int i, j, k, l;
  int x_grid = ((map_width/10)-1);
 //map_width must be a multiple of 10, not truncated
  int y_grid = ((map_height/10)-1); //same as above
  //the following form the bounds of the range where land is guaranteed
  //to be placed--between the lower and upper  quartiles
  int x_low = (x_grid * 0.25); //truncates
  int x_high = (x_grid * 0.75); //truncates
  int y_low = (y_grid * 0.25); //truncates
  int y_high = (y_grid * 0.75); //truncates
  //the first row & column, and last row & column are always guaranteed 
  //to be water in any map (minimum dimensions are 100x100 in tiles).
  for(i = 0; i < map_width; ++i)
    {
      for(j = 0; j < map_height; ++j)
	{
          if( ((i/10)!=0) && ((j/10)!=0) && ((i/10)!=x_grid) &&
	      ((j/10)!= y_grid))  //if not reserved for water on edge of world
	    {
              if( ((i/10)>x_low) && ((i/10)<x_high) && ((j/10)>y_low) &&
		  ((j/10)<y_high)) //if i, j in 100% land zone
		{
                  map[i][j] = land;
		}
	      else if( ((i % 10)==0) && ((j % 10)==0) ) //trigger cells
		{
                  if( (rand() % 100) > 74) //25% chance of making a land block
		    {
		      for(k = 0; k < 10; ++k)
			{
			  for(l = 0; l < 10; ++l)
			    {
			      map[i+k][j+l] = land;
			    }
			}
		    }
		}
	    }
	}
    }
  for(i = 0; i < map_width; ++i)  //this refinement could be a separate function
    {
      for(j = 0; j < map_height; ++j)
	{
          if( ((i/10)!=0) && ((j/10)!=0) && ((i/10)!=x_grid) &&
	      ((j/10)!= y_grid))  //if not reserved for water on edge of world
	    {
              if( ((i/10)<=x_low) || ((i/10)>=x_high) || ((j/10)<=y_low) ||
		  ((j/10)>=y_high)) //if i, j NOT in 100% land zone
		{
		  if( ((i%10)==0) && ((j%10)==0) && (map[i][j] == land))
		    {  //if it's a key cell that's also been filled in
		      const int hold_i = i;
		      const int hold_j = j;
		      if((rand() % 2)==1)
			{
   		          while( (i/10) <= x_low)
			    {
                              i += 10;
			      if(map[i][j] != land)
				{
				  for(k = 0; k < 10; ++k)
				    {
				      for(l = 0; l < 10; ++l)
					{
					  map[i+k][j+l] = land;
					}
				    }
				}
			    }
			  i = hold_i;
			  while( (i/10) >= x_high)
			    {
                              i -= 10;
			      if(map[i][j] != land)
				{
				  for(k = 0; k < 10; ++k)
				    {
				      for(l = 0; l < 10; ++l)
					{
					  map[i+k][j+l] = land;
					}
				    }
				}
			    }
			  i = hold_i;
			  while( (j/10) <= y_low)
			    {
                              j += 10;
			      if(map[i][j] != land)
				{
				  for(k = 0; k < 10; ++k)
				    {
				      for(l = 0; l < 10; ++l)
					{
					  map[i+k][j+l] = land;
					}
				    }
				}
			    }
			  j = hold_j;
			  while( (j/10) >= y_high)
			    {
                              j -= 10;
			      if(map[i][j] != land)
				{
				  for(k = 0; k < 10; ++k)
				    {
				      for(l = 0; l < 10; ++l)
					{
					  map[i+k][j+l] = land;
					}
				    }
				}
			    }
			  j = hold_j;
			}
		      else
			{
			  while( (j/10) <= y_low)
			    {
                              j += 10;
			      if(map[i][j] != land)
				{
				  for(k = 0; k < 10; ++k)
				    {
				      for(l = 0; l < 10; ++l)
					{
					  map[i+k][j+l] = land;
					}
				    }
				}
			    }
			  j = hold_j;
			  while( (j/10) >= y_high)
			    {
                              j -= 10;
			      if(map[i][j] != land)
				{
				  for(k = 0; k < 10; ++k)
				    {
				      for(l = 0; l < 10; ++l)
					{
					  map[i+k][j+l] = land;
					}
				    }
				}
			    }
			  j = hold_j;
			  while( (i/10) <= x_low)
			    {
                              i += 10;
			      if(map[i][j] != land)
				{
				  for(k = 0; k < 10; ++k)
				    {
				      for(l = 0; l < 10; ++l)
					{
					  map[i+k][j+l] = land;
					}
				    }
				}
			    }
			  i = hold_i;
			  while( (i/10) >= x_high)
			    {
                              i -= 10;
			      if(map[i][j] != land)
				{
				  for(k = 0; k < 10; ++k)
				    {
				      for(l = 0; l < 10; ++l)
					{
					  map[i+k][j+l] = land;
					}
				    }
				}
			    }
			  i = hold_i;
			}
		    }
		}
	    }
	}
    }
}

void Map::place_sand_grass()
{
  int i, j;

  int state = grass;

  for(i = 0; i < (map_width); ++i)
    {
      for(j = 0; j < (map_height); ++j)
	{
	  if( (map[i][j] == land) && (state == grass)) //if tile is land & grass
	    {
	      if((rand()%20) > 18)  //5% chance will change to sand
	        {
	          map[i][j] = sand;  //3 will denote sand; 2 will be grass
		  state = sand;
           	}
	      else
	        {
	          map[i][j] = grass;
	        }
	    }
          if( (map[i][j] == land) && (state == sand)) //if tile is land & sand
	    {
	      if((rand()%10) > 8) //10% chance it will change to grass  
		{
		  map[i][j] = grass;
		  state = grass;
		}
	      else
		{
		  map[i][j] = sand;
		}
	    }
	}
    }
  //this next pass is to get rid of isolated, one-column wide swathes of 
  //pure grass and pure sand tiles--it looks janky otherwise
  for(i = 0; i < (map_width); ++i)
    {
      for(j = 0; j < (map_height); ++j)
	{
	  if(map[i][j] == sand)
	    {
	      if( (map[i+1][j] == grass) && (map[i-1][j] == grass) )
		{
		  map[i][j] = grass;
		}
	      else if( (map[i][j+1] == grass) && (map[i][j-1] == grass) )
		{
		  map[i][j] = grass;
		}
	    }
	  else if(map[i][j] == grass)
	    {
              if( (map[i+1][j] == sand) && (map[i-1][j] == sand) )
		{
		  map[i][j] = sand;
		}
	      else if( (map[i][j+1] == sand) && (map[i][j-1] == sand) )
		{
		  map[i][j] = sand;
		}
	    }
	}
    }
}


void Map::refine_land_textures()  //placing appropriate transition tiles
{
  //Ugly and hard-coded, don't say I didn't warn you
  for(int i = 0; i < map_width; ++i)
    {
      for(int j = 0; j < map_height; ++j)
	{
          if(map[i][j] == grass)
	    {
	      if( (map[i-1][j] == sand) && 
                  ((map[i+1][j] == grass) || (map[i+1][j] == water)) )
		{
		  if(map[i][j-1] == sand)
		    {
		      map[i][j] = thick_down_right;
		      
		    }
		  else if(map[i][j+1] == sand)
		    {
		      map[i][j] = thick_up_right;
		      
		    }
		  else
		    {
		      map[i][j] = right;
		      
		    }
		}
              if( (map[i+1][j] == sand) && 
                  ((map[i-1][j] == grass) || (map[i-1][j] == water)) )
		{
		  if(map[i][j-1] == sand)
		    {
		      map[i][j] = thick_down_left;
		      
		    }
		  else if(map[i][j+1] == sand)
		    {
		      map[i][j] = thick_up_left;
		      
		    }
		  else
		    {
		      map[i][j] = left;
		      
		    }
		}
              if( (map[i][j-1] == sand) &&
                  ((map[i][j+1] == grass) || (map[i][j+1] == water)) )
		{
		  if(map[i+1][j] == sand)
		    {
		      map[i][j] = thick_down_left;
		      
		    }
		  else if(map[i-1][j] == sand)
		    {
		      map[i][j] = thick_down_right;
		      
		    }
		  else 
		    {
		      map[i][j] = lower;
		      
		    }
		}
	      if( (map[i][j+1] == sand) && 
		  ((map[i][j-1] == grass) || (map[i][j-1] == water)) )
		{
		  if(map[i-1][j] == sand)
		    {
		      map[i][j] = thick_up_right;
		      
		    }
		  else if(map[i+1][j] == sand)
		    {
		      map[i][j] = thick_up_left;
		      
		    }
		  else
		    {
		      map[i][j] = upper;
		      
		    }
		}
	    }
	}
    }
}

void Map::refine_land_texture_corners()
{
  for(int i = 0; i < map_width; ++i)
    {
      for(int j = 0; j < map_height; ++j)
	{
          if(map[i][j] == grass)
	    {
	      if( (map[i-1][j-1] == sand) && (map[i-1][j] != grass) 
		  && (map[i][j-1] != grass) )
		{
		  map[i][j] = lower_right;
		  
		}
	      if( (map[i-1][j+1] == sand) && (map[i-1][j] != grass) 
		  && (map[i][j+1] != grass) )
		{
		  map[i][j] = upper_right;
		  
		}
	      if( (map[i+1][j-1] == sand) && (map[i+1][j] != grass)
		  && (map[i][j-1] != grass) )
		{
		  map[i][j] = lower_left;
		  
		}
	      if( (map[i+1][j+1] == sand) && (map[i+1][j] != grass)
		  && (map[i][j+1] != grass) )
		{
		  map[i][j] = upper_left;
		  
		}
	    }
	}
    }

}


void Map::fill_in_water()  //this fills in landlocked water
{
  int i, j;
  for(i = 0; i < map_width; ++i)
    {
      for(j = 0; j < map_height; ++j)
	{
	  if(map[i][j] == 0);
	  {
	    int left_sum = 0, right_sum = 0, up_sum = 0, down_sum = 0;
	    int hold_i = i;
	    int hold_j = j;
	    while(i > -1)
	      {
		left_sum += map[i][j];
		--i;
	      }
	    i = hold_i;
	    while(i < map_width)
	      {
		right_sum += map[i][j];
		++i;
	      }
	    i = hold_i;
	    while(j > -1)
	      {
		up_sum += map[i][j];
		--j;
	      }
	    j = hold_j;
	    while(j < map_height)
	      {
		down_sum += map[i][j];
		++j;
	      }
	    j= hold_j;
	    if(left_sum && right_sum && up_sum && down_sum)
	      {
		map[i][j] = 1;
	      }
	  }
	}
    }
}

int Map::get_tile_X( int index )
{
  int tileX = mapTiles[index]->get_X();
  return tileX;
}

int Map::get_tile_Y( int index )
{
  int tileY = mapTiles[index]->get_Y();
  return tileY;
}

int Map::get_tile_type( int index )
{
  int tile_type = mapTiles[index]->get_type();
  return tile_type;
}

SDL_Rect Map::get_tile_rect( int index )
{
  SDL_Rect tile_rect = mapTiles[index]->get_box();
  return tile_rect;
}
  
void Map::display(SDL_Surface *destination)
{
  SDL_Rect tileBox;
  for(int i = 0; i < (map_width * map_height); ++i)
    {
      tileBox = mapTiles[i]->get_box();
      mapTiles[i]->toBackground(get_surface(), destination, map_tile_clips, tileBox.x, tileBox.y);
    }
}

int Map::get_start_X()
{
  for(int i = 0; i < (map_width * map_height); ++i)
  {
    if(mapTiles[i]->get_type() != 0)
      return mapTiles[i]->get_X();
  }
}

int Map::get_start_Y()
{
  for(int i = 0; i < (map_width * map_height); ++i)
  {
    if(mapTiles[i]->get_type() != 0)
      return mapTiles[i]->get_Y();
  }
}

bool Map::is_coast( int index )
{
    if( mapTiles[index + (map_width)]->get_type() == 0 )
        return true;
    if( mapTiles[index - (map_width)]->get_type() == 0 )
        return true;
    if( mapTiles[index + 1]->get_type() == 0 )
        return true;
    if( mapTiles[index - 1]->get_type() == 0 )
        return true;
    return false;
}
