#ifndef MYSURFACE_H
#define MYSURFACE_H

class MySurface		//holds SDL_Surface pointer and dimensions of surface
{
	public:
	    MySurface(SDL_Surface*, int, int, int, int);
	    void setSurface( SDL_Surface* );
	    SDL_Surface* getSurface();
	    void setDims(int,int,int,int);
	    SDL_Rect getDims();
	    int getWidth();
	    int getHeight();
	    int getX();
	    int getY();
	private:
	    SDL_Rect surfaceDimensions;
	    SDL_Surface* mySurface;
};

MySurface::MySurface(SDL_Surface* aSurface, int x, int y, int height, int width)
{
    mySurface = aSurface;
    surfaceDimensions.x = x;
    surfaceDimensions.y = y;
    surfaceDimensions.h = height;
    surfaceDimensions.w = width;
}

void MySurface::setSurface( SDL_Surface* aSurface)
{
    mySurface = aSurface;
}

SDL_Surface* MySurface::getSurface()
{
    return mySurface;
}

void MySurface::setDims( int x, int y, int height, int width )
{
    surfaceDimensions.x = x;
    surfaceDimensions.y = y;
    surfaceDimensions.h = height;
    surfaceDimensions.w = width;
}

SDL_Rect MySurface::getDims()
{
    return surfaceDimensions;
}

int MySurface::getX()
{
    return surfaceDimensions.x;
}

int MySurface::getY()
{
    return surfaceDimensions.y;
}

int MySurface::getHeight()
{
    return surfaceDimensions.h;
}

int MySurface::getWidth()
{
    return surfaceDimensions.w;
}

#endif
