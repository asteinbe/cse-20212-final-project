/* Interface for the Inventory class, used in the Player class via composition
 * Fundamentals of Computing II - Spring 2014 - Prof. Emrich
 */

#ifndef INVENTORY_H
#define INVENTORY_H

#include<SDL/SDL.h>
#include<SDL/SDL_image.h>
#include<SDL/SDL_ttf.h>
#include<vector>

extern SDL_Surface *load_image( std::string filename, int r, int g, int b );
extern void apply_surface( int x, int y, SDL_Surface* source, SDL_Surface* destination, SDL_Rect* clip);
extern SDL_Rect camera;

class Inventory
{

public:

  Inventory();
  ~Inventory();

  int get_slot_number(int whichSlot) const;//returns # items in specified slot
  void add_slot_number(int whichSlot, int howMuch); //adds to the specified slot
  void decrease_slot_number(int whichSlot, int howMuch);
  
  int get_slot_max() const;   //returns maximum # items per slot, same for all
                             //slots for simplicity
  void set_slot_max(int max);      //sets the max # items per slot

  void display(SDL_Surface * toScreen);
                                          //displays inventory items 
                                          //& number on the bottom of screen

  bool has_axe() const; //true if player has axe, returns axe
  bool has_pickaxe() const;//true if player has a pickaxe, returns pickaxe
  bool has_spear() const; //true if the player has a spear, returns spear

private:

  std::vector<int> slot;  //hold the number of items in each slot
  SDL_Surface * inventory_sprite_sheet; //surface of all inventory sprites
  SDL_Rect inventory_clips[10];  //clips for the inventory sprite sheet
  void set_inventory_clips();   //sets the clips for the inventory sprites
  //each clip corresponds to the item in the slots (so slots are fixed)
  //e.g. a berry will always be in slot 0, sticks in slot 1, etc.  
  TTF_Font * inventory_font; //TrueType Font pointer
  SDL_Surface * numbers[10]; //array of surfaces that display number of items
                            //in corresponding slot
  SDL_Color inventory_font_color;  //color of the font used to demarcate amount
  int slot_max;      //maximum number of items the regular slots can hold
  bool axe;     //true if player has an axe (slot 7)      keyboard: 8
  bool pickaxe;  //true if player has a pickaxe (slot 8)  keyboard : 9
  bool spear;   //true if player has a spear (slot 9)   keyboard: 0
};

#endif
