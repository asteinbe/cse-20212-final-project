#include "MySurface.h"
#include <iostream>
#include <vector>

extern SDL_Surface* background;
extern SDL_Surface* screen;
extern void apply_surface( int x, int y, SDL_Surface* source, SDL_Surface* destination, SDL_Rect* clip );
extern bool check_collision( SDL_Rect A, SDL_Rect B );

#ifndef ENTITIES_H
#define ENTITIES_H

class Entities //Entities contains a vector of MySurface pointers, and functions to manipulate the vector
{
    public:
	Entities(SDL_Surface*,int,int);
	bool checkValid(int,int,SDL_Rect);
	void addEntity(int,int);
	void removeEntity(int);
	void showEntities();
	int getEntityHeight();
	int getEntityWidth();
	int getEntityCount();
	SDL_Rect getEntityRect(int);
	std::vector<MySurface*> getEntities();
    private:
	SDL_Surface* entitySurface;
        int entityHeight;
        int entityWidth;
	std::vector<MySurface*> entities;
};

Entities::Entities(SDL_Surface* surfaceIn, int width, int height)
{
    entitySurface = surfaceIn;
    entityWidth = width;
    entityHeight = height;
}

bool Entities::checkValid(int checkX, int checkY, SDL_Rect tempTrying) //If there isn't collision between the SDL_Rect given and the SDL_Rects in the entities vector, return true
{
    int i;
    for(i=0;i<entities.size();i++)
    {
	if( check_collision( entities[i]->getDims(), tempTrying ) )
        {
            return false;
        }
    }
    return true;
}

void Entities::addEntity(int addX, int addY) //If the location is valid, place the entity at the given coordinates
{
    SDL_Rect tempTrying = {addX, addY, entityHeight, entityWidth}; //entity we're trying to add
    if( checkValid( addX, addY, tempTrying ) )
    {
        MySurface* toAdd = new MySurface(entitySurface, addX, addY, entityHeight, entityWidth);
        entities.push_back(toAdd);
    }
}

void Entities::removeEntity(int index)
{
    entities.erase(entities.begin() + index);
}

void Entities::showEntities()
{
    int i;
    for(i=0;i<entities.size();i++)
    {
	apply_surface(entities[i]->getX(), entities[i]->getY(), entitySurface, background, NULL);
    }
}

int Entities::getEntityHeight()
{
    return entityHeight;
}

int Entities::getEntityWidth()
{
    return entityWidth;
}

int Entities::getEntityCount()
{
    return entities.size();
}

SDL_Rect Entities::getEntityRect(int index)
{
    return entities[index]->getDims();
}

std::vector<MySurface*> Entities::getEntities()
{
    return entities;
}

#endif
