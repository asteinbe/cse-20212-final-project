#include "Entities.h"
#include "Tile.h"
#include "Map.h"
#include "Character.h"
#include "SDL/SDL_mixer.h"
#include "SDL/SDL_mixer.h"
#include <cmath>
#include <cstdlib>

//Passing cbox into the constructor should be temporary

#ifndef ANIMAL_H
#define ANIMAL_H

class Animal : public Character
{
    public:
        //Initializes the variables
        Animal(int,int,int,int,int,SDL_Surface*);
	// sets the velocity of the animal depending on if the player is close
	// to it or not
        void SetMovement(Player player);
	// chases the player
	void Chase(Player player);
	void didKillPlayer(Player*);
        void set_clips();
	void AddChasingRadius(int value);	// changes the bear's chasing radius
    private:
	// the animal will continuously chase the player when this is true
	bool isChasing;
	// Movement is not constantly changed.  This keeps track of when it was
	// changed last
	int timeMovementWasChangedLast;
	int chasingRadius;	// the radius in which the bear will chase you
        Mix_Chunk * bearRoar;  //sound effect for bear roaring
};

Animal::Animal(int initX, int initY, int height, int width, int i, SDL_Surface* self) 
	: Character(initX, initY, height, width, self)
{
    upAnimations = 4;
    downAnimations = 4;
    leftAnimations = 4;
    rightAnimations = 4;
    isChasing = false;
    timeMovementWasChangedLast = SDL_GetTicks();
    chasingRadius = 300;
    bearRoar = NULL;
    bearRoar = Mix_LoadWAV("grizzly-bear.wav");
    if(!bearRoar) std::cout << "Error loading bear roar" << std::endl;
}

// Sets the movement of the animal.  If the player is close enough, the animal
// chases him
void Animal::SetMovement(Player player)
{
    int time = SDL_GetTicks();
    // If it has not been two seconds since movement was last changed
    // and the bear is not chasing the player, don't do anything
    if(time - timeMovementWasChangedLast < 2000)
    {
	if(isChasing == true)
	{
	    Chase(player);
	}
	return;
    }

    // resets movement
    xVelocity = yVelocity = 0;

    // calculates the distance between the animal and the player
    float distance = sqrt(pow(player.getX() - getX(), 2) + pow(player.getY() - getY(), 2));

    // If the player is close enough, chase him
    if(distance < 300)
    {
        Mix_PlayChannel(-1, bearRoar, 0);
	isChasing = true;
	Chase(player);
    }
    // Otherwise, move randomly up, down, left, or right
    else
    {
	int randInt = rand() % 4;
	if(randInt == 0)
	    xVelocity = 5;
	else if(randInt == 1)
	    yVelocity = 5;
	else if(randInt == 2)
	    xVelocity = -5;
	else
	    yVelocity = -5;
	isChasing = false;
    }
    timeMovementWasChangedLast = time;
}

// Chases the player
void Animal::Chase(Player player)
{
	// sets the animal's direction of movement towards the player
	xVelocity = player.getX() - getX();
	yVelocity = player.getY() - getY();

	// scales the speed of movement down so the animal is not going too fast
	// or too slow
	int scalingFactor = sqrt(pow(xVelocity, 2) + pow(yVelocity, 2));
	xVelocity = xVelocity / scalingFactor;
	yVelocity = yVelocity / scalingFactor;

	// Finally, changes the speed of the movement so the animal runs at the
	// correct speed
	xVelocity *= 10;
	yVelocity *= 10;
}

void Animal::didKillPlayer( Player* playerPtr )
{
	if( check_collision( playerPtr->getCollideRect(), collide_block ) )
	{
		playerPtr->killPlayer();
	}
}

void Animal::set_clips()
{
    //Clip the sprites
    clipsDown[ 0 ].x = 0;
    clipsDown[ 0 ].y = 0;
    clipsDown[ 0 ].w = characterDims.w;
    clipsDown[ 0 ].h = characterDims.h;

    clipsDown[ 1 ].x = characterDims.w;
    clipsDown[ 1 ].y = 0;
    clipsDown[ 1 ].w = characterDims.w;
    clipsDown[ 1 ].h = characterDims.h;

    clipsDown[ 2 ].x = characterDims.w * 2;
    clipsDown[ 2 ].y = 0;
    clipsDown[ 2 ].w = characterDims.w;
    clipsDown[ 2 ].h = characterDims.h;

    clipsDown[ 3 ].x = characterDims.w * 3;
    clipsDown[ 3 ].y = 0;
    clipsDown[ 3 ].w = characterDims.w;
    clipsDown[ 3 ].h = characterDims.h;

    clipsLeft[ 0 ].x = 0;
    clipsLeft[ 0 ].y = characterDims.h;
    clipsLeft[ 0 ].w = characterDims.w;
    clipsLeft[ 0 ].h = characterDims.h;

    clipsLeft[ 1 ].x = characterDims.w;
    clipsLeft[ 1 ].y = characterDims.h;
    clipsLeft[ 1 ].w = characterDims.w;
    clipsLeft[ 1 ].h = characterDims.h;

    clipsLeft[ 2 ].x = characterDims.w * 2;
    clipsLeft[ 2 ].y = characterDims.h;
    clipsLeft[ 2 ].w = characterDims.w;
    clipsLeft[ 2 ].h = characterDims.h;

    clipsLeft[ 3 ].x = characterDims.w * 3;
    clipsLeft[ 3 ].y = characterDims.h;
    clipsLeft[ 3 ].w = characterDims.w;
    clipsLeft[ 3 ].h = characterDims.h;

    clipsRight[ 0 ].x = 0;
    clipsRight[ 0 ].y = characterDims.h * 2;
    clipsRight[ 0 ].w = characterDims.w;
    clipsRight[ 0 ].h = characterDims.h;

    clipsRight[ 1 ].x = characterDims.w;
    clipsRight[ 1 ].y = characterDims.h * 2;
    clipsRight[ 1 ].w = characterDims.w;
    clipsRight[ 1 ].h = characterDims.h;

    clipsRight[ 2 ].x = characterDims.w * 2;
    clipsRight[ 2 ].y = characterDims.h * 2;
    clipsRight[ 2 ].w = characterDims.w;
    clipsRight[ 2 ].h = characterDims.h;

    clipsRight[ 3 ].x = characterDims.w * 3;
    clipsRight[ 3 ].y = characterDims.h * 2;
    clipsRight[ 3 ].w = characterDims.w;
    clipsRight[ 3 ].h = characterDims.h;

    clipsUp[ 0 ].x = 0;
    clipsUp[ 0 ].y = characterDims.h * 3;
    clipsUp[ 0 ].w = characterDims.w;
    clipsUp[ 0 ].h = characterDims.h;

    clipsUp[ 1 ].x = characterDims.w;
    clipsUp[ 1 ].y = characterDims.h * 3;
    clipsUp[ 1 ].w = characterDims.w;
    clipsUp[ 1 ].h = characterDims.h;

    clipsUp[ 2 ].x = characterDims.w * 2;
    clipsUp[ 2 ].y = characterDims.h * 3;
    clipsUp[ 2 ].w = characterDims.w;
    clipsUp[ 2 ].h = characterDims.h;

    clipsUp[ 3 ].x = characterDims.w * 3;
    clipsUp[ 3 ].y = characterDims.h * 3;
    clipsUp[ 3 ].w = characterDims.w;
    clipsUp[ 3 ].h = characterDims.h;
}

// adds "value" to the animal's chasing radius
void Animal::AddChasingRadius(int value)
{
	chasingRadius += value;
}
#endif
