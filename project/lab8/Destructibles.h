#include "MySurface.h"
#include <iostream>
#include <vector>

extern SDL_Surface* background;
extern SDL_Surface* screen;
extern void apply_surface( int x, int y, SDL_Surface* source, SDL_Surface* destination, SDL_Rect* clip );
extern bool check_collision( SDL_Rect A, SDL_Rect B );

#ifndef DESTRUCTIBLES_H
#define DESTRUCTIBLES_H

class Destructibles
{
    //Entity, but with a few extra functions/variables for health, drops, etc. (should have used inheritance)
    public:
	Destructibles(SDL_Surface*,SDL_Surface*,int,int,int,int);
	bool checkValid(int,int,SDL_Rect);
	int addDestructible(int,int); //returns 1 if successful
	void showDestructibles();
	int getBeforeHeight();
	int getBeforeWidth();
	int getAfterHeight();
	int getAfterWidth();
	int getHealth(int);	// gets the health of a destructible at an index
	std::vector<MySurface*> getDestructibles();
	void SetHealth(int, int);	// sets the health of a destructible
    private:
	SDL_Surface* beforeSurface; //surface before destroyed
	SDL_Surface* afterSurface; //surface after destroyed
        int beforeHeight;
        int beforeWidth;
        int afterHeight;
        int afterWidth;
	std::vector<MySurface*> destructibles;
	std::vector<int> destructibleHealth;
};

Destructibles::Destructibles(SDL_Surface* surfaceInBefore, SDL_Surface* surfaceInAfter, int bWidth, int bHeight, int aWidth, int aHeight)
{
    beforeSurface = surfaceInBefore;
    afterSurface = surfaceInAfter;
    beforeWidth = bWidth;
    beforeHeight = bHeight;
    afterWidth = aWidth;
    afterHeight = aHeight;
}

bool Destructibles::checkValid(int checkX, int checkY, SDL_Rect tempTrying)
{
    int i;
    for(i=0;i<destructibles.size();i++)
    {
	if( check_collision( destructibles[i]->getDims(), tempTrying ) )
        {
            return false;
        }
    }
    return true;
}

int Destructibles::addDestructible(int addX, int addY)
{
    SDL_Rect tempTrying = {addX, addY, beforeHeight, beforeWidth}; //destructible we're trying to add (only the full health form can be added)
    if( checkValid( addX, addY, tempTrying ) )
    {
        MySurface* toAdd = new MySurface(beforeSurface, addX, addY, beforeHeight, beforeWidth);
        destructibles.push_back(toAdd);
	destructibleHealth.push_back(2);
    }
}

void Destructibles::showDestructibles()
{
    int i;
    for(i=0;i<destructibles.size();i++)
    {
	if(destructibleHealth[i]!=0)
	{
		apply_surface(destructibles[i]->getX(), destructibles[i]->getY(), beforeSurface, background, NULL);
	}
	else
	{
		apply_surface(destructibles[i]->getX(), destructibles[i]->getY(), afterSurface, background, NULL);
	}
    }
}

int Destructibles::getBeforeHeight()
{
    return beforeHeight;
}

int Destructibles::getBeforeWidth()
{
    return beforeWidth;
}

int Destructibles::getAfterHeight()
{
    return afterHeight;
}

int Destructibles::getAfterWidth()
{
    return afterWidth;
}

std::vector<MySurface*> Destructibles::getDestructibles()
{
    return destructibles;
}

// gets the health of a destructible at a particular index
int Destructibles::getHealth(int index)
{
    return destructibleHealth[index];
}

// sets the health of the destructible at index "index" to value "value"
void Destructibles::SetHealth(int index, int value)
{
    if(destructibleHealth[index] == 0)
    {
	// if the value was changed from zero to a positive number, set the dimensions of the object to
	// be the dimensions of the beforeSurface, and set its position to make it look right.
	if(value != 0)
	{
	    destructibles[index]->setDims(
		destructibles[index]->getX() + afterWidth/2 - beforeWidth/2, 
		destructibles[index]->getY() + afterHeight - beforeHeight, 
		beforeWidth, beforeHeight);
	}
    }
    else
    {
	// if the value was changed from a positive number to zero, set the dimensions of the object to
	// be the dimensions of the afterSurface, and set its position to make it look right.
    	if(value == 0)
	{
	    destructibles[index]->setDims(
		destructibles[index]->getX() - afterWidth/2 + beforeWidth/2, 
		destructibles[index]->getY() - afterHeight + beforeHeight, 
		afterWidth, afterHeight);
	}
    }

    // Sets the health of the destructible
    destructibleHealth[index] = value;
}

#endif
