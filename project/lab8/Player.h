#include "Entities.h"
#include "Tile.h"
#include "Map.h"
#include "Timer.h"
#include "SDL/SDL_mixer.h"
#include "WorldObjects.h"
#include "Character.h"
#include "Inventory.h"

//first tent doesn't give a shit

extern SDL_Surface * darkness;

#ifndef PLAYER_H
#define PLAYER_H

//The player
class Player : public Character
{
    public:
        //Initializes the variables
        Player(int,int,int,int,SDL_Surface*,SDL_Surface*,SDL_Surface*,Inventory*);
	bool placeObject(int, int, int, WorldObjects*, Map);
	//Handles input
        void handle_events(WorldObjects*, Map);
	WorldObjects getPickups(WorldObjects);
	// sets the camera to be centered on the player's position
        void set_camera();
	void showCharacter();
	void showPlacedObjects();
        void set_clips();
	void spawn(WorldObjects*, Map);
	void SwingSword(WorldObjects*);	// The character swings the sword and breaks any
					// rocks or trees in his path.  It does not effect
					// wild animals though
	void ChangeHunger(int);		// changes the player's hunger
	void ChangeTiredness(int);	// changes the player's tiredness
	void ChangeStatus();		// decraments the player's hunger and tiredness as the
					// player becomes naturally hungry and tired
	int GetHunger();		
	int GetTiredness();
	int GetDay();
	void killPlayer();
	bool IsAlive();			// returns true if the player is alive
    private:
	SDL_Surface* characterSwinging;	// the sprite sheet for when the character
					// is swinging his sword

	bool swingingSword;		// true if the character is swinging his sword,
					// false otherwise.
	int swordSharpness;		// the number of times the player can cut
					// objects before the sword becomes "dull"
					// and can't cut anything

	// the areas of the sword swinging sprite sheet
	SDL_Rect clipsSwingingUp[6];
	SDL_Rect clipsSwingingDown[6];
	SDL_Rect clipsSwingingRight[6];
	SDL_Rect clipsSwingingLeft[6];

	// areas of the dead sprite
	SDL_Rect clipsLayingDown;

	Inventory* myInventory;		// the inventory (this uses composition)
	bool alive;			// the player is alive while this is true

	int hunger;			// hunger starts out at 100, and if it
					// reaches 0, you die.
	int tiredness;			// tiredness works the same way as hunger
	int day;			// the day counter.  This increments when the
					// player goes to sleep (places down a
					// tent)
	int timeBetweenStatusChanges;	// the amount of time the player can go before
					// his hunger and tiredness are naturally decreased
	int timeStatusChangedLast;	// the last time the player's hunger and 
					// tireness were naturally decreased
	Mix_Chunk * sleep;        //stores the sleeping sound effect    
        Mix_Chunk * death;        //stores the death sound effect
};

Player::Player(int initX, int initY, int height, int width, SDL_Surface* self, SDL_Surface* swinging, SDL_Surface* tentSurface, Inventory* theInventory) 
	: Character(initX, initY, height, width, self)
{

    // Sets the sword swinging sprite sheet
    characterSwinging = swinging;

    // Place the entities we want to use in the placedObjects vector
    Entities tents( tentSurface, 128, 138 );
    placedObjects.push_back( tents );

    swingingSword = false;
    swordSharpness = 25;

    upAnimations = 5;
    downAnimations = 5;
    leftAnimations = 5;
    rightAnimations = 5;

    collide_block.h = characterDims.h / 2;
    collide_block.y = characterDims.y + (characterDims.h - collide_block.h);

    myInventory = theInventory;
    alive = true;

    // adds the sword sharpness to inventory position 2
    myInventory->add_slot_number(2, swordSharpness);

    hunger = 100;
    tiredness = 100;
    day = 0;

    timeBetweenStatusChanges = 4000;	// 4 seconds
    timeStatusChangedLast = 0;

    sleep = NULL;
    sleep = Mix_LoadWAV("sleep.wav");
    if(!sleep)  std::cout << "Error loading sleep.wav" << std::endl;

    death = NULL;
    death = Mix_LoadWAV("death.wav");
    if(!death) std::cout << "Error loading death.wav" << std::endl;     
}

bool Player::placeObject( int index, int tryX, int tryY, WorldObjects* WorldObjectsPtr, Map myMap )
{
    int i;
    int j;
    int tryHeight = placedObjects[index].getEntityHeight();
    int tryWidth = placedObjects[index].getEntityWidth();
    SDL_Rect tempTrying = {tryX, tryY, tryHeight, tryWidth};
    for(i=0;i<placedObjects.size();i++) //iterates through all Entities objects in placedObjects
    {
	if(placedObjects[i].checkValid(tryX, tryY, tempTrying)==false)
        {
	    return false;
	}
	for(j=0;j<placedObjects[i].getEntityCount();j++) 
	{
	    if(checkAllCollisions( WorldObjectsPtr, myMap, tempTrying ) )
	    {
		return false;
	    }
	}
    }
    placedObjects[index].addEntity(tryX, tryY);
    return true;
}

void Player::handle_events(WorldObjects* WorldObjectsPtr, Map myMap)
{
    //If a key was pressed
    if( event.type == SDL_KEYDOWN )
    {
        //Set the velocity if an arrow key was pressed, swing the sword if the
	// space key was pressed, or make a tent if 1 was pressed and the player
	// has enough resources
        switch( event.key.keysym.sym )
        {
            case SDLK_RIGHT:	xVelocity += characterDims.w / 3; break;
            case SDLK_LEFT:	xVelocity -= characterDims.w / 3; break;
            case SDLK_UP:	yVelocity -= characterDims.h / 6; break;
            case SDLK_DOWN:	yVelocity += characterDims.h / 6; break;
	    case SDLK_SPACE:	SwingSword(WorldObjectsPtr); break;
				// if there are more than 10 logs in the inventory
	    case SDLK_1:	
		if(myInventory->get_slot_number(0) >= 10)
		{
			// if a tent can be placed, place it,
			// and reduce the number of logs by 10
			if(placeObject(0, collide_block.x, collide_block.y - placedObjects[0].getEntityHeight(), WorldObjectsPtr, myMap))
        		{
				myInventory->decrease_slot_number(0, 10);
				// the player rested, so set his
	    			// tiredness back to 100
                                SDL_Rect * dummy_ptr = NULL;
                                int alpha = 0;
                                Timer fade;
                                fade.start();
                                Mix_PlayChannel(-1, sleep, 0); 
                                while(alpha < 100)
				  {
                                    if(fade.get_ticks() > 500)
				      {
                                        alpha += 5;
					fade.start();
				      }
                                    SDL_SetAlpha(darkness, SDL_SRCALPHA, alpha);
                                    apply_surface(0, 0, darkness, screen,
                                                  dummy_ptr);
                                    SDL_Flip(screen);
				  }
				tiredness = 100;
				// Increment the day
				day++;
			}
		}	
		break;
	    case SDLK_2:
		// If there are more than 10 stones in the inventory
		if(myInventory->get_slot_number(1) >= 10)
		{
			// Resharpen the player's sword
			swordSharpness = 25;
			myInventory->decrease_slot_number(1, 10);
			// set the position in the inventory corresponding to sword
			// sharpness (2) to equal 25
			myInventory->add_slot_number(2, 25 - myInventory->get_slot_number(2));
		}
		break;
		
	}
    }

    //If a key was released
    else if( event.type == SDL_KEYUP )
    {
        //Set the velocity
        switch( event.key.keysym.sym )
        {
            case SDLK_RIGHT: xVelocity -= characterDims.w / 3; break;
            case SDLK_LEFT: xVelocity += characterDims.w / 3; break;
            case SDLK_UP: yVelocity += characterDims.h / 6; break;
            case SDLK_DOWN: yVelocity -= characterDims.h / 6; break;
        }
    }
}

WorldObjects Player::getPickups(WorldObjects myWorldObjects)
{
    std::vector<Entities> myPickups = myWorldObjects.getPickupsVec();
    int i;
    int j;
    for(i=0;i<myPickups.size();i++) //iterates through all Entities objects in placedObjects
    {
        for(j=0;j<myPickups[i].getEntities().size();j++) //for each of these objects, iterate through the contained vector of MySurface pointers
        {
	    if( check_collision( ( myPickups[i].getEntities())[j]->getDims(), collide_block ) )
            {
		// add values to the player's inventory
		
		// If the player ran into a berry, add 25 to his hunger.
		if(i == 0)
			ChangeHunger(25);	

		// remove the pickup from the map (and the Pickups vector)
		myWorldObjects.removePickup(i, j);
            }
        }
    }
    return myWorldObjects;
}

void Player::showPlacedObjects()
{
    int i;
    for(i=0;i<placedObjects.size();i++)
    {
        placedObjects[i].showEntities();
    }
}

void Player::set_camera()
{
    //Center the camera over the dot
    camera.x = ( characterDims.x + characterDims.w / 2 ) - screenWidth / 2;
    camera.y = ( characterDims.y +  characterDims.h / 2 ) - screenHeight / 2;

    //Keep the camera in bounds.
    if( camera.x < 0 )
    {
        camera.x = 0;
    }
    if( camera.y < 0 )
    {
        camera.y = 0;
    }
    if( camera.x > levelWidth - camera.w )
    {
        camera.x = levelWidth - camera.w;
    }
    if( camera.y > levelHeight - camera.h )
    {
        camera.y = levelHeight - camera.h;
    }
}

// Draws the character to the screen
void Player::showCharacter()
{
    // if the character is dead, display him as such
    if(!alive)
    {
     	apply_surface( characterDims.x - camera.x, characterDims.y - camera.y, character, screen, &clipsLayingDown );
	return;
    }

    if(swingingSword)
    {
	// If the character has not finished 5 frames of swinging his sword,
	// keep on swinging
	if(frame <= 5)
	{
	    // shows the correct animation depending on the direction the character
	    // is facing and his frame
	    if( status == CharacterDown )
    	    {
		apply_surface( characterDims.x - camera.x, characterDims.y - camera.y, characterSwinging, screen, &clipsSwingingDown[ frame ] );
	    }
	    else if( status == CharacterUp )
	    {
		apply_surface( characterDims.x - camera.x, characterDims.y - camera.y - 20, characterSwinging, screen, &clipsSwingingUp[ frame ] );
	    }
	    else if( status == CharacterLeft )
	    {
		apply_surface( characterDims.x - camera.x - 40, characterDims.y - camera.y, characterSwinging, screen, &clipsSwingingLeft[ frame ] );
	    }
	    else if( status == CharacterRight )
	    {
		apply_surface( characterDims.x - camera.x, characterDims.y - camera.y, characterSwinging, screen, &clipsSwingingRight[ frame ] );
	    }
	    frame++;
	    return;
	}
	// if the character is done swinging, move.
	else
	{
	    swingingSword = false;
	    stopped = false;
	}
    }	
    if( yVelocity < 0 && abs(yVelocity) >= abs(xVelocity))
    {
     	//Set the animation to up
        status = CharacterUp;

        //Move to the next frame in the animation
        frame++;
    }
    else if( yVelocity > 0 && abs(yVelocity) >= abs(xVelocity))
    {
     	//Set the animation to down
        status = CharacterDown;

        //Move to the next frame in the animation
        frame++;
    }
    else if( xVelocity < 0 )
    {
     	//Set the animation to left
        status = CharacterLeft;

        //Move to the next frame in the animation
        frame++;
    }
    else if( xVelocity > 0 )
    {
     	//Set the animation to right
        status = CharacterRight;
	//Move to the next frame in the animation
        frame++;
    }
    //If Character is standing
    else
    {
     	//Restart the animation
        frame = 0;
    }
    //Loop the animation
    if( status == CharacterRight )
    {
     	if ( frame >= rightAnimations )
        {
            frame = 0;
        }
    }
    else if( status == CharacterLeft )
    {
     	if ( frame >= leftAnimations )
        {
            frame = 0;
        }
    }
    else if( status == CharacterUp )
    {
     	if ( frame >= upAnimations )
        {
            frame = 0;
        }
    }
    else if( status == CharacterDown )
    {
     	if ( frame >= downAnimations )
        {
            frame = 0;
        }
    }

    //Show the character
    if( status == CharacterDown )
    {
     	apply_surface( characterDims.x - camera.x, characterDims.y - camera.y, character, screen, &clipsDown[ frame ] );
    }
    else if( status == CharacterUp )
    {
     	apply_surface( characterDims.x - camera.x, characterDims.y - camera.y, character, screen, &clipsUp[ frame ] );
    }
    else if( status == CharacterLeft )
    {
     	apply_surface( characterDims.x - camera.x, characterDims.y - camera.y, character, screen, &clipsLeft[ frame ] );
    }
    else if( status == CharacterRight )
    {
     	apply_surface( characterDims.x - camera.x, characterDims.y - camera.y, character, screen, &clipsRight[ frame ] );
    }
}

void Player::set_clips()
{
    //Clip the sprites

    clipsLeft[ 0 ].x = characterDims.w * 4;
    clipsLeft[ 0 ].y = 0;
    clipsLeft[ 0 ].w = characterDims.w;
    clipsLeft[ 0 ].h = characterDims.h;

    clipsLeft[ 1 ].x = 0;
    clipsLeft[ 1 ].y = 0;
    clipsLeft[ 1 ].w = characterDims.w;
    clipsLeft[ 1 ].h = characterDims.h;

    clipsLeft[ 2 ].x = characterDims.w;
    clipsLeft[ 2 ].y = 0;
    clipsLeft[ 2 ].w = characterDims.w;
    clipsLeft[ 2 ].h = characterDims.h;

    clipsLeft[ 3 ].x = characterDims.w * 2;
    clipsLeft[ 3 ].y = 0;
    clipsLeft[ 3 ].w = characterDims.w;
    clipsLeft[ 3 ].h = characterDims.h;

    clipsLeft[ 4 ].x = characterDims.w * 3;
    clipsLeft[ 4 ].y = 0;
    clipsLeft[ 4 ].w = characterDims.w;
    clipsLeft[ 4 ].h = characterDims.h;

    clipsRight[ 0 ].x = characterDims.w * 5;
    clipsRight[ 0 ].y = 0;
    clipsRight[ 0 ].w = characterDims.w;
    clipsRight[ 0 ].h = characterDims.h;

    clipsRight[ 1 ].x = characterDims.w * 6;
    clipsRight[ 1 ].y = 0;
    clipsRight[ 1 ].w = characterDims.w;
    clipsRight[ 1 ].h = characterDims.h;

    clipsRight[ 2 ].x = characterDims.w * 7;
    clipsRight[ 2 ].y = 0;
    clipsRight[ 2 ].w = characterDims.w;
    clipsRight[ 2 ].h = characterDims.h;

    clipsRight[ 3 ].x = characterDims.w * 8;
    clipsRight[ 3 ].y = 0;
    clipsRight[ 3 ].w = characterDims.w;
    clipsRight[ 3 ].h = characterDims.h;

    clipsRight[ 4 ].x = characterDims.w * 9;
    clipsRight[ 4 ].y = 0;
    clipsRight[ 4 ].w = characterDims.w;
    clipsRight[ 4 ].h = characterDims.h;

    clipsDown[ 0 ].x = characterDims.w * 14;
    clipsDown[ 0 ].y = 0;
    clipsDown[ 0 ].w = characterDims.w;
    clipsDown[ 0 ].h = characterDims.h;

    clipsDown[ 1 ].x = characterDims.w * 10;
    clipsDown[ 1 ].y = 0;
    clipsDown[ 1 ].w = characterDims.w;
    clipsDown[ 1 ].h = characterDims.h;

    clipsDown[ 2 ].x = characterDims.w * 11;
    clipsDown[ 2 ].y = 0;
    clipsDown[ 2 ].w = characterDims.w;
    clipsDown[ 2 ].h = characterDims.h;

    clipsDown[ 3 ].x = characterDims.w * 12;
    clipsDown[ 3 ].y = 0;
    clipsDown[ 3 ].w = characterDims.w;
    clipsDown[ 3 ].h = characterDims.h;

    clipsDown[ 4 ].x = characterDims.w * 13;
    clipsDown[ 4 ].y = 0;
    clipsDown[ 4 ].w = characterDims.w;
    clipsDown[ 4 ].h = characterDims.h;

    clipsUp[ 0 ].x = characterDims.w * 19;
    clipsUp[ 0 ].y = 0;
    clipsUp[ 0 ].w = characterDims.w;
    clipsUp[ 0 ].h = characterDims.h;

    clipsUp[ 1 ].x = characterDims.w * 15;
    clipsUp[ 1 ].y = 0;
    clipsUp[ 1 ].w = characterDims.w;
    clipsUp[ 1 ].h = characterDims.h;

    clipsUp[ 2 ].x = characterDims.w * 16;
    clipsUp[ 2 ].y = 0;
    clipsUp[ 2 ].w = characterDims.w;
    clipsUp[ 2 ].h = characterDims.h;

    clipsUp[ 3 ].x = characterDims.w * 17;
    clipsUp[ 3 ].y = 0;
    clipsUp[ 3 ].w = characterDims.w;
    clipsUp[ 3 ].h = characterDims.h;

    clipsUp[ 4 ].x = characterDims.w * 18;
    clipsUp[ 4 ].y = 0;
    clipsUp[ 4 ].w = characterDims.w;
    clipsUp[ 4 ].h = characterDims.h;

    clipsLayingDown.x = characterDims.w * 20;
    clipsLayingDown.y = 0;
    clipsLayingDown.w = 65;
    clipsLayingDown.h = characterDims.h;

    clipsSwingingUp[0].x = 0;
    clipsSwingingUp[0].y = 0;
    clipsSwingingUp[0].w = (characterDims.w + 4);
    clipsSwingingUp[0].h = (characterDims.h + 20);

    clipsSwingingUp[1].x = (characterDims.w + 4);
    clipsSwingingUp[1].y = 0;
    clipsSwingingUp[1].w = (characterDims.w + 4);
    clipsSwingingUp[1].h = (characterDims.h + 20);

    clipsSwingingUp[2].x = (characterDims.w + 4) * 2;
    clipsSwingingUp[2].y = 0;
    clipsSwingingUp[2].w = (characterDims.w + 4);
    clipsSwingingUp[2].h = (characterDims.h + 20);

    clipsSwingingUp[3].x = (characterDims.w + 4) * 3;
    clipsSwingingUp[3].y = 0;
    clipsSwingingUp[3].w = (characterDims.w + 4);
    clipsSwingingUp[3].h = (characterDims.h + 20);

    clipsSwingingUp[4].x = (characterDims.w + 4) * 4;
    clipsSwingingUp[4].y = 0;
    clipsSwingingUp[4].w = (characterDims.w + 4);
    clipsSwingingUp[4].h = (characterDims.h + 20);

    clipsSwingingUp[5].x = (characterDims.w + 4) * 5;
    clipsSwingingUp[5].y = 0;
    clipsSwingingUp[5].w = (characterDims.w + 4);
    clipsSwingingUp[5].h = (characterDims.h + 20);

    clipsSwingingDown[0].x = (characterDims.w + 4) * 6;
    clipsSwingingDown[0].y = 0;
    clipsSwingingDown[0].w = (characterDims.w + 4);
    clipsSwingingDown[0].h = (characterDims.h + 20);

    clipsSwingingDown[1].x = (characterDims.w + 4) * 7;
    clipsSwingingDown[1].y = 0;
    clipsSwingingDown[1].w = (characterDims.w + 4);
    clipsSwingingDown[1].h = (characterDims.h + 20);

    clipsSwingingDown[2].x = (characterDims.w + 4) * 8;
    clipsSwingingDown[2].y = 0;
    clipsSwingingDown[2].w = (characterDims.w + 4);
    clipsSwingingDown[2].h = (characterDims.h + 20);

    clipsSwingingDown[3].x = (characterDims.w + 4) * 9;
    clipsSwingingDown[3].y = 0;
    clipsSwingingDown[3].w = (characterDims.w + 4);
    clipsSwingingDown[3].h = (characterDims.h + 20);

    clipsSwingingDown[4].x = (characterDims.w + 4) * 10;
    clipsSwingingDown[4].y = 0;
    clipsSwingingDown[4].w = (characterDims.w + 4);
    clipsSwingingDown[4].h = (characterDims.h + 20);

    clipsSwingingDown[5].x = (characterDims.w + 4) * 11;
    clipsSwingingDown[5].y = 0;
    clipsSwingingDown[5].w = (characterDims.w + 4);
    clipsSwingingDown[5].h = (characterDims.h + 20);

    clipsSwingingRight[0].x = (characterDims.w + 40) * 7;
    clipsSwingingRight[0].y = (characterDims.h + 20);
    clipsSwingingRight[0].w = (characterDims.w + 40);
    clipsSwingingRight[0].h = (characterDims.h + 4);

    clipsSwingingRight[1].x = (characterDims.w + 40) * 8;
    clipsSwingingRight[1].y = (characterDims.h + 20);
    clipsSwingingRight[1].w = (characterDims.w + 40);
    clipsSwingingRight[1].h = (characterDims.h + 4);

    clipsSwingingRight[2].x = (characterDims.w + 40) * 9;
    clipsSwingingRight[2].y = (characterDims.h + 20);
    clipsSwingingRight[2].w = (characterDims.w + 40);
    clipsSwingingRight[2].h = (characterDims.h + 4);

    clipsSwingingRight[3].x = (characterDims.w + 40) * 10;
    clipsSwingingRight[3].y = (characterDims.h + 20);
    clipsSwingingRight[3].w = (characterDims.w + 40);
    clipsSwingingRight[3].h = (characterDims.h + 4);

    clipsSwingingRight[4].x = (characterDims.w + 40) * 11;
    clipsSwingingRight[4].y = (characterDims.h + 20);
    clipsSwingingRight[4].w = (characterDims.w + 40);
    clipsSwingingRight[4].h = (characterDims.h + 4);

    clipsSwingingRight[5].x = (characterDims.w + 40) * 12;
    clipsSwingingRight[5].y = (characterDims.h + 20);
    clipsSwingingRight[5].w = (characterDims.w + 40);
    clipsSwingingRight[5].h = (characterDims.h + 4);

    clipsSwingingLeft[0].x = 0;
    clipsSwingingLeft[0].y = (characterDims.h + 20);
    clipsSwingingLeft[0].w = (characterDims.w + 40);
    clipsSwingingLeft[0].h = (characterDims.h + 4);

    clipsSwingingLeft[1].x = (characterDims.w + 40);
    clipsSwingingLeft[1].y = (characterDims.h + 20);
    clipsSwingingLeft[1].w = (characterDims.w + 40);
    clipsSwingingLeft[1].h = (characterDims.h + 4);

    clipsSwingingLeft[2].x = (characterDims.w + 40) * 2;
    clipsSwingingLeft[2].y = (characterDims.h + 20);
    clipsSwingingLeft[2].w = (characterDims.w + 40);
    clipsSwingingLeft[2].h = (characterDims.h + 4);

    clipsSwingingLeft[3].x = (characterDims.w + 40) * 3;
    clipsSwingingLeft[3].y = (characterDims.h + 20);
    clipsSwingingLeft[3].w = (characterDims.w + 40);
    clipsSwingingLeft[3].h = (characterDims.h + 4);

    clipsSwingingLeft[4].x = (characterDims.w + 40) * 4;
    clipsSwingingLeft[4].y = (characterDims.h + 20);
    clipsSwingingLeft[4].w = (characterDims.w + 40);
    clipsSwingingLeft[4].h = (characterDims.h + 4);

    clipsSwingingLeft[5].x = (characterDims.w + 40) * 5;
    clipsSwingingLeft[5].y = (characterDims.h + 20);
    clipsSwingingLeft[5].w = (characterDims.w + 40);
    clipsSwingingLeft[5].h = (characterDims.h + 4);
}

void Player::spawn(WorldObjects* WorldObjectsPtr, Map myMap)
{
    int tryX = myMap.get_start_X();
    int tryY = myMap.get_start_Y();
    characterDims.x = tryX;
    collide_block.x = characterDims.x;
    characterDims.y = tryY;
    collide_block.y = characterDims.y + (characterDims.h - collide_block.h);
    while ( checkAllCollisions( WorldObjectsPtr, myMap, collide_block) )
    {	
        characterDims.x += 40; // tile size
        collide_block.x = characterDims.x;
	if( characterDims.x >= myMap.get_width()*40 ) //if max X is reached
	{	
            characterDims.x = 0; // tile size
            collide_block.x = characterDims.x;
    	    characterDims.y += 40;
    	    collide_block.y = characterDims.y + (characterDims.h - collide_block.h);
	}
    }
}   

void Player::SwingSword(WorldObjects* WorldObjectsPtr)
{
	// if the sword is dull, it cannot cut anything.  Swing the sword, but
	// don't do anything else
	if(swordSharpness == 0)
	{
		return;
	}

	// creats a rectangle that will store the path the sword will
	// take when it is swung.
	SDL_Rect temp;
	
	// gets the vector of destructalbes from WorldObjectsPtr 
	std::vector<Destructibles> destructiblesVector = WorldObjectsPtr->getBreakablesVec();

	// sets the rectangle to the correct coordinates based on the direction
	// the character is facing
	if(status == CharacterDown)
	{
		temp.x = collide_block.x;
		temp.y = collide_block.y + 20;
		temp.w = collide_block.w;
		temp.h = collide_block.h;
	}
	else if(status == CharacterUp)
	{
		temp.x = collide_block.x;
		temp.y = characterDims.y;
		temp.w = collide_block.w;
		temp.h = collide_block.h;
	}
	else if(status == CharacterLeft)
	{
		temp.x = collide_block.x - 30;
		temp.y = collide_block.y;
		temp.w = collide_block.w;
		temp.h = collide_block.h;
	}
	else if(status == CharacterRight)
	{
		temp.x = collide_block.x + 30;
		temp.y = collide_block.y;
		temp.w = collide_block.w;
		temp.h = collide_block.h;
	}

	// If the rectangle of the sword collides with any destructibles,
	// destroy that destructible
	for(int i=0; i<destructiblesVector.size(); i++)
	{
	    // loops through all destructible objects of type "i"
	    for(int j=0; j<destructiblesVector[i].getDestructibles().size(); j++)
	    {
		// if the destructible is not already destroyed
		if(destructiblesVector[i].getHealth(j) != 0)
		{
		    // if the sword collides with the destructible, destroy it
		    if(check_collision(temp, destructiblesVector[i].getDestructibles()[j]->getDims()))
		    {
			WorldObjectsPtr -> SetDestructibleHealth(i, j, 0);
			// If the player cut down a tree, add a log to his
			// inventory
			if(i == 0)
			{
			    myInventory->add_slot_number(0,1);
			}
			// If the player cut a stone, add some ore to his
			// inventory
			if(i == 1)
			{
			    myInventory->add_slot_number(1,1);
			}

			// decrease the sword's sharpness by 1
			swordSharpness--;
			myInventory->decrease_slot_number(2, 1);
		    }
		}
	    }
	}

	// sets the swinging sword variable to true, and sets the velocity
	// of the player to zero (since the player should not move if he is
	// swinging his sword)
	swingingSword = true;
	stopped = true;
	frame = 0;
}

// changes the player's hunger value by "amount".  This amount can be positive
// or negative.  The player's hunger cannot go above 100, and if it reaches 
// zero or below, the player dies.
void Player::ChangeHunger(int amount)
{
	hunger += amount;
	if(hunger > 100)
		hunger = 100;
	else if(hunger <= 0)
		alive = false;
}


// changes the player's tiredness value by "amount".  This amount can be positive
// or negative.  The player's tiredness cannot go above 100, and if it reaches 
// zero or below, the player dies.
void Player::ChangeTiredness(int amount)
{
	tiredness += amount;
	if(tiredness > 100)
		tiredness = 100;
	else if(tiredness <= 0)
		alive = false;
}

// Changes the player's tiredness and hunger after in specific time
// increments
void Player::ChangeStatus()
{
	int time = SDL_GetTicks();
	if(time - timeBetweenStatusChanges > timeStatusChangedLast)
	{
		ChangeHunger(-5);
		ChangeTiredness(-5);
		timeStatusChangedLast = time;
	}
}

int Player::GetHunger()
{
	return hunger;
}

int Player::GetTiredness()
{
	return tiredness;
}

int Player::GetDay()
{
	return day;
}

bool Player::IsAlive()
{
	return alive;
}

void Player::killPlayer()
{
	alive = false;
        Mix_PlayChannel(-1, death, 0);
}

#endif
