/* Interface: The generic tile class used in creating the map.
 *
 */

extern void apply_surface( int x, int y, SDL_Surface* source, SDL_Surface* destination, SDL_Rect* clip);

#ifndef TILE_H 
#define TILE_H

#include<SDL/SDL.h>
#include<SDL/SDL_image.h>
#include <iostream>

class Tile
{

public:

  Tile(int tileX, int tileY, int tileType); //constructor
  void toBackground(SDL_Surface *fromMap, SDL_Surface *toBackground, SDL_Rect *cp, int x, int y);
  int get_type();            //returns the tile's type
  SDL_Rect get_box();       //returns the tile's collision box
  int get_X();
  int get_Y();

private:

  SDL_Rect box;        //the offsets and dimensions of the tiles, & collision
  int type;            //what type of tile it is

};


#endif
