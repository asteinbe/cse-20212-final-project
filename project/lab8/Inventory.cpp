/* Implementation for the Inventory class used in Player class via composition
 * Fundamentals of Computing II - Spring 2014 - Prof. Emrich
 */

#include<iostream>
#include<SDL/SDL.h>
#include<SDL/SDL_image.h>
#include<SDL/SDL_ttf.h>
#include<vector>
#include<string>
#include<sstream>
#include"Inventory.h"

Inventory::Inventory()  
{
  inventory_sprite_sheet = load_image("inventory_pine.png", 255, 255, 255);
  set_inventory_clips();

  if(TTF_Init() == -1)
    {
      std::cout << "Problem initializing TTF (fonts) for inventory." 
		<< std::endl;
    }

  inventory_font = TTF_OpenFont("ilits.ttf", 18);
  //2nd argument is font size
  inventory_font_color = {0, 0, 0}; //list init color of font to red

  for(int i = 0; i < 3; ++i)
    slot.push_back(0);

  set_slot_max(30);
}

Inventory::~Inventory()
{
  SDL_FreeSurface(inventory_sprite_sheet);
  TTF_CloseFont(inventory_font);
}

//note that 'whichSlot' is an int rather than unsigned--I'd rather make the
//extra check now than potentially deal with wrap-around bugs later
int Inventory::get_slot_number(int whichSlot) const
{
  if( (whichSlot >= 0) && (whichSlot < 3) )
    return slot[whichSlot];
  else
    return 0;                               //note the error value if there
                                        //are empty inventory slot bugs later
}

void Inventory::set_inventory_clips()
{
      inventory_clips[0].x = 40*3;
      inventory_clips[0].y = 0;
      inventory_clips[0].w = inventory_clips[0].h = 40;

      inventory_clips[1].x = 40*4;
      inventory_clips[1].y = 0;
      inventory_clips[1].w = inventory_clips[1].h = 40;

      inventory_clips[2].x = 40*7;
      inventory_clips[2].y = 0;
      inventory_clips[2].w = inventory_clips[2].h = 40;
}


void Inventory::add_slot_number(int whichSlot, int howMuch) 
{
  if( (whichSlot >= 0) && (whichSlot < 3) )
    {
      if(howMuch > 0)
        {
          if( (slot[whichSlot]+howMuch) <= slot_max)
	    {
	      slot[whichSlot]+=howMuch;
	    }
	}
    }
}

void Inventory::decrease_slot_number(int whichSlot, int howMuch)
{
  if( (whichSlot >= 0) && (whichSlot < 3) )
    {
      if(howMuch > 0)
        {
          if( (slot[whichSlot]-howMuch) >= 0)
            {
              slot[whichSlot]-=howMuch;
	    }
	  else
	    {
	      slot[whichSlot] = 0;
	    }
	}
    }
}

int Inventory::get_slot_max() const
{
  return slot_max;
}

void Inventory::set_slot_max(int max)
{
  if(max > 0)
    slot_max = max;
}

void Inventory::display(SDL_Surface * toScreen)  
{
  //we'll just hard code in '3' instead of using slot.size() to 
  //avoid std::vector<int>::size_type; also, it's hardcoded above
  SDL_Rect *dummy_ptr = NULL;
  for(int i = 0; i < 3; ++i)
    {
      //apply inventory slot item image across bottom of screen (40x40 images)
	  apply_surface( (i+1)*160 - 20, 428, 
			 inventory_sprite_sheet, toScreen, &inventory_clips[i]);
	  std::stringstream amount;
	  amount << slot[i];
	  numbers[i]=TTF_RenderText_Solid(inventory_font, amount.str().c_str(), 
					    inventory_font_color);
	  apply_surface((i+1)*160 - 20, 452, numbers[i], toScreen, dummy_ptr);
	  SDL_FreeSurface(numbers[i]);
          numbers[i] = new SDL_Surface;
    }
}

bool Inventory::has_axe() const
{
  return axe;
}

bool Inventory::has_pickaxe() const
{
  return pickaxe;
}

bool Inventory::has_spear() const
{
  return spear;
}
