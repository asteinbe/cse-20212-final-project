/* Interface for the map class, which randomly generates the map upon 
 * instantiation.
 */

extern void apply_surface( int x, int y, SDL_Surface* source, SDL_Surface* destination, SDL_Rect* clip);

extern SDL_Surface *load_image( std::string filename, int r, int g, 
                                int b );

#ifndef MAP_H
#define MAP_H

#include<SDL/SDL.h>
#include<SDL/SDL_image.h>
#include<vector>
#include"Tile.h"

class Map 
{

public:

  Map(int width = 100, int height = 100); //constructor
                               //note that width & height are in number of tiles

  ~Map();  //The destructor for the map, frees private map surface 

  //  void display() const; //displays the respective map region on the screen

  int get_width() const;   //returns map width
  int get_height() const;  //returns map height

  void display(SDL_Surface *destination); 
  //displays the tile elements stored in mapTiles on screen

  SDL_Surface* get_surface() const;  //returns map surface

  int get_tile_X(int);
  int get_tile_Y(int);
  int get_tile_type(int);
  SDL_Rect get_tile_rect(int);

  int get_start_X(); //Gets the X coordinate of the first land tile
  int get_start_Y(); //Gets the Y coordinate of the first land tile

  bool is_coast(int);

private:

  int map_width;  //the number of tiles in the horizontal direction
  int map_height;  //the number of tiles in the vertical direction
  Tile **mapTiles; //will point to first element of an array of Tile pointers
  SDL_Rect map_tile_clips[16]; //clipping the texture sheet-16 tile types
  void set_map_tile_clips(); //sets the clippings 
  void set_width(int width); //securely sets width
  void set_height(int height);  //securely sets height
  std::vector<std::vector<int> > map; //contains numeric code for the map 
  void generate_vector();  //generates the 0-valued vector 
  void generate_land_mass(); //generates land shape & boundaries 
  void place_sand_grass(); //just changes land tiles to grass/sand #'s
  void fill_in_water(); //fills in all enclosed water tiles
  void refine_land_textures(); //adds transition tiles between sand and grass
  void refine_land_texture_corners(); //transition tiles on corner bounds
  SDL_Surface *map_surface;  //the map tilesheet
  //These next definitions are for the tiles clippings, clearer than macros
  //and type safe!
  static const int water = 0;
  static const int land = 1;
  static const int grass = 2;
  static const int sand = 3;
  static const int upper_left = 4;
  static const int upper = 5;
  static const int upper_right = 6;
  static const int right = 7;
  static const int lower_right = 8;
  static const int lower = 9;
  static const int lower_left = 10;
  static const int left = 11;
  static const int thick_up_left = 12;
  static const int thick_down_left = 13;
  static const int thick_up_right = 14;
  static const int thick_down_right = 15;
};

#endif
