//Andrew Steinbergs
//Lab 8
//Testing git push

//Vector of entities in main/new class: world objects

//The headers
#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include "Character.h"
#include "Player.h"
#include "Animal.h"
#include "Timer.h"
#include "MySurface.h"
#include "Entities.h"
#include "Map.h"
#include "Tile.h"
#include "WorldObjects.h"
#include "Inventory.h"
#include "SDL/SDL_mixer.h"
#include <iostream>
#include <cstdlib>
#include <string>
#include <vector>
#include "SDL/SDL_ttf.h"
#include <sstream>

using namespace std;

//Screen attributes
const int screenWidth = 640;
const int screenHeight = 480;
const int screenBPP = 32;

// Width and height of the map in pixels
const int levelWidth = 4000;
const int levelHeight = 4000;

//The frames per second
const int framesPerSecond = 10;

SDL_Rect camera = { 0, 0, screenWidth, screenHeight };

std::vector<Entities> placedObjects;

//The surfaces
SDL_Surface *will = NULL;
SDL_Surface *willSwinging = NULL;
SDL_Surface *screen = NULL;
SDL_Surface *tent = NULL;
SDL_Surface *bear = NULL;
SDL_Surface *berry = NULL;
SDL_Surface *tree = NULL;
SDL_Surface *stump = NULL;
SDL_Surface *rock = NULL;
SDL_Surface *rubble = NULL;
SDL_Surface *background = NULL;
SDL_Surface *darkness = NULL;

SDL_Surface *hungerSurface = NULL;
SDL_Surface *tirednessSurface = NULL;
SDL_Surface *daySurface = NULL;
TTF_Font *font = NULL;

SDL_Surface * introScreen = NULL; //maroon background of title & instr. screens 
SDL_Color introFontColor = {250, 253, 105}; //kind of yellow-ish
TTF_Font * introFont = NULL;  //font of intro screens

//The event structure
SDL_Event event;

// color of the font
SDL_Color textColor = {255, 255, 255};


SDL_Surface *load_image( std::string filename, int r=0, int g=255, int b=255 )
{
  //The image that's loaded
  SDL_Surface* loadedImage = NULL;

  //The optimized surface that will be used
  SDL_Surface* optimizedImage = NULL;

  //Load the image
  loadedImage = IMG_Load( filename.c_str() );

  //If the image loaded
  if( loadedImage != NULL )
    {
      //Create an optimized surface
      optimizedImage = SDL_DisplayFormat( loadedImage );

      //Free the old surface
      SDL_FreeSurface( loadedImage );

      //If the surface was optimized
      if( optimizedImage != NULL )
        {
	  //Color key surface
	  SDL_SetColorKey( optimizedImage, SDL_SRCCOLORKEY, SDL_MapRGB( optimizedImage->format, r, g, b ) );
        }
    }

  //Return the optimized surface
  return optimizedImage;
}

void apply_surface( int x, int y, SDL_Surface* source, SDL_Surface* destination, SDL_Rect* clip = NULL)
{
  //Holds offsets
  SDL_Rect offset;

  //Get offsets
  offset.x = x;
  offset.y = y;

  //Blit
  SDL_BlitSurface( source, clip, destination, &offset );
}

bool init()
{
  //Initialize all SDL subsystems
  if( SDL_Init( SDL_INIT_EVERYTHING ) == -1 )
    {
      return false;
    }

  //Set up the screen
  screen = SDL_SetVideoMode( screenWidth, screenHeight, screenBPP, SDL_SWSURFACE );

  //If there was an error in setting up the screen
  if( screen == NULL )
    {
      return false;
    }
  if(TTF_Init() == -1)
    {
      return false;
    }
  if(Mix_OpenAudio(22050, MIX_DEFAULT_FORMAT, 2, 4096) == -1)
    {
      return false;
    }
  //Set the window caption
  SDL_WM_SetCaption( "Stayin' Alive", NULL );

  //If everything initialized fine
  return true;
}

bool load_files()
{
  // load the font
  font = TTF_OpenFont( "ilits.ttf", 24);

  //Load the sprite sheet
  will = load_image( "will.png" );

  willSwinging = load_image( "willswing.png" );

  tent = load_image ( "tent.png" );

  bear = load_image ( "bear.png" );

  berry = load_image( "berries.png" );

  tree = load_image( "tree.png" );

  stump = load_image( "stump.png" );

  rock = load_image( "rock.png" );

  rubble = load_image( "rubble.png" );

  darkness = load_image("darkness.png");
    
  //If there was a problem in loading the sprite
  if( will == NULL || willSwinging == NULL || bear == NULL )
    {
      return false;
    }

  if( tent == NULL )
    {
      return false;
    }

  if( berry == NULL || tree == NULL || stump == NULL || rock == NULL || rubble == NULL)
    {
      return false;
    }
 
  //If everything loaded fine
  return true;
}

void clean_up()
{
  //Free all surfaces
  SDL_FreeSurface( will );
  SDL_FreeSurface( willSwinging );
  SDL_FreeSurface( tent );
  SDL_FreeSurface( bear );
  SDL_FreeSurface( berry );
  SDL_FreeSurface( tree );
  SDL_FreeSurface( stump );
  SDL_FreeSurface( rock );
  SDL_FreeSurface( rubble );
  SDL_FreeSurface( hungerSurface );
  SDL_FreeSurface( tirednessSurface );
  SDL_FreeSurface( daySurface );

  //Close font
  TTF_CloseFont(font);

  //quit TTF
  TTF_Quit();
 
  Mix_CloseAudio();

  //Quit SDL
  SDL_Quit();
}

bool check_collision( SDL_Rect A, SDL_Rect B )
{
  //The sides of the rectangles
  int leftA, leftB;
  int rightA, rightB;
  int topA, topB;
  int bottomA, bottomB;

  //Calculate the sides of rect A
  leftA = A.x;
  rightA = A.x + A.w;
  topA = A.y;
  bottomA = A.y + A.h;

  //Calculate the sides of rect B
  leftB = B.x;
  rightB = B.x + B.w;
  topB = B.y;
  bottomB = B.y + B.h;

  //If any of the sides from A are outside of B
  if( bottomA <= topB )
    {
      return false;
    }

  if( topA >= bottomB )
    {
      return false;
    }

  if( rightA <= leftB )
    {
      return false;
    }

  if( leftA >= rightB )
    {
      return false;
    }

  //If none of the sides from A are outside B
  return true;
}

// Displays the current hunger of the player at the top of the screen
void DisplayHunger(SDL_Surface*, Player player)
{
  std::stringstream hunger;

  //Convert the player's hunger to a string
  hunger << "Hunger: " << player.GetHunger();

  //Render the hunger surface
  hungerSurface = TTF_RenderText_Solid( font, hunger.str().c_str(), textColor );

  //Apply the hunger surface to the top left of the screen
  apply_surface(  (screenWidth / 3 - hungerSurface->w) / 2 , 0, hungerSurface, screen );
}

// Displays the current tiredness of the player at the top of the screen
void DisplayTiredness(SDL_Surface*, Player player)
{
  std::stringstream tiredness;

  //Convert the player's tiredness to a string
  tiredness << "Tiredness: " << player.GetTiredness();

  //Render the tiredness surface
  tirednessSurface = TTF_RenderText_Solid( font, tiredness.str().c_str(), textColor );

  //Apply the tiredness surface to the top right of the screen
  apply_surface( ( screenWidth - (screenWidth / 3 + tirednessSurface->w) / 2), 0, tirednessSurface, screen );
}

// Displays the current day at the top of the screen
void DisplayDay(SDL_Surface*, Player player)
{
  std::stringstream day;

  //Convert the player's day to a string
  day << "Day: " << player.GetDay();

  //Render the day surface
  daySurface = TTF_RenderText_Solid( font, day.str().c_str(), textColor );

  //Apply the day surface to the top right of the screen
  apply_surface( screenWidth / 2 - daySurface->w / 2, 0, daySurface, screen );
}

// Displays a game over screen once a player gets killed
//void DisplayGameOver(SDL_Surface*, Player player)

SDL_Surface* Get_Sub_Surface(SDL_Surface* metaSurface, int x, int y, int width, int height)
{
  // Create an SDL_Rect with the area of the surface
  SDL_Rect area;
  area.x = x;
  area.y = y;
  area.w = width;
  area.h = height;

  // Set the RGBA mask values.
  Uint32 r, g, b, a;
#if SDL_BYTEORDER == SDL_BIG_ENDIAN
  r = 0xff000000;
  g = 0x00ff0000;
  b = 0x0000ff00;
  a = 0x000000ff;
#else
  r = 0x000000ff;
  g = 0x0000ff00;
  b = 0x00ff0000;
  a = 0xff000000;
#endif

  //Create a new surface via SDL_CreateRGBSurface() then convert it to match the display's format
  SDL_Surface* subSurface = SDL_DisplayFormat(SDL_CreateRGBSurface(SDL_SWSURFACE, width, height, 32, r, g, b, a));

  //apply the area from the meta surface onto the whole of the sub surface.
  SDL_BlitSurface(metaSurface, &area, subSurface, 0);

  return subSurface;
}



/******************************************************************************/



int main( int argc, char* args[] )
{
  //Initialize
  if( init() == false )
    {
      return 1;
    }
  if(load_files() == false) return 33;
  bool titleQuit = false;
  introScreen = load_image("maroon_title.png");
  if(introScreen == NULL) return 23;
  introFont = TTF_OpenFont("ilits.ttf", 96);
  TTF_Font * smallIntroFont = TTF_OpenFont("ilits.ttf", 48);
  SDL_Surface *introLine1 = TTF_RenderText_Solid(introFont,"Stayin'", 
						    introFontColor);
  SDL_Surface *introLine2 = TTF_RenderText_Solid(introFont, "Alive", 
						 introFontColor);
  SDL_Surface *introLine3 = TTF_RenderText_Solid(smallIntroFont, 
						 "Press Space to Continue", 
						 introFontColor);
  Mix_Music *titleMusic = Mix_LoadMUS("staying_alive.wav");
  
  
  Timer blinking;  //for blinking display
  blinking.start();
  Mix_PlayMusic(titleMusic, -1);
  while(titleQuit == false)
    {
      while(SDL_PollEvent(&event))
	{
	  if(event.type == SDL_KEYDOWN)
	    {
	      if(event.key.keysym.sym == SDLK_SPACE)
		{
		  titleQuit = true;
		}
	    }
	}

      apply_surface(0, 0, introScreen, screen);
      apply_surface((screenWidth-introLine1->w)/2, introLine1->h, introLine1, 
		    screen);
      apply_surface((screenWidth-introLine2->w)/2, 2*(introLine1->h), 
                     introLine2, screen);
      if(blinking.get_ticks() < 1000)
	{
	  apply_surface((screenWidth-introLine3->w)/2,
                        (screenHeight-introLine3->h), introLine3, screen);
	}
      else if(blinking.get_ticks() > 2000)
	{
	  blinking.start();
	}
      
      if(SDL_Flip(screen) == -1)
        {
          return 999;
        }
    }
  Mix_HaltMusic();
  Mix_FreeMusic(titleMusic);

/*****************************************************************************/
  //game loop quit flag
  bool quit = false;

  //Load the files
  if( load_files() == false )
    {
      return 1;
    }

  //The frame rate regulator
  Timer fps;

  background = Get_Sub_Surface(background, 0, 0, levelWidth, levelHeight);

  //making and filling the map

  Map TheMap(100, 100);

  TheMap.display( background );

  // all generated objects on the map
  WorldObjects myWorldObjects( TheMap );

  myWorldObjects.generateObjects();

  // The inventory that the player will have
  Inventory playerInventory;
    
  // Creates a player object and spawns the player on the map
  Player player( 0, 0, 76, 42, will, willSwinging, tent, &playerInventory); //The character
  player.spawn(&myWorldObjects, TheMap);

  Animal WildBear(0, 0, 56, 56, 0, bear);
    
  WildBear.spawn(&myWorldObjects, TheMap);

  //Clip the sprite sheet
  player.set_clips();

  WildBear.set_clips();

  // the current day
  int day;

  Mix_Music *gameMusic = Mix_LoadMUS("birds.wav");
  Mix_PlayMusic(gameMusic, -1);

  //While the user hasn't quit
  while( quit == false )
    {
      while( player.IsAlive() )
	{
	  //Update the screen
	  if( SDL_Flip( screen ) == -1 )
            {
	      return 1;
            }

	// get the value of the previous day
	day = player.GetDay();
	  //Start the frame timer
	  fps.start();

	  //While there's events to handle
	  while( SDL_PollEvent( &event ) )
            {
	      //Handle events for the player
	      player.handle_events(&myWorldObjects, TheMap);

	      //If the user has Xed out the window
	      if( event.type == SDL_QUIT )
                {
		  //Kill the player and quit the program
		  player.ChangeHunger(-10000);
		  quit = true;
                }
            }

	// If a day has passed, add 150 to the bear's chasing radius
	if(player.GetDay() != day)
	{
		WildBear.AddChasingRadius(150);
	}

	  //Move the player
	  player.move(&myWorldObjects, TheMap);

	  //remove objects the player picks up from the world objects
	  myWorldObjects = player.getPickups(myWorldObjects);

	  //Set the bear's movement toward the player
	  WildBear.SetMovement(player);

	  WildBear.move(&myWorldObjects, TheMap);

	  player.set_camera();

	  //Apply background
	  TheMap.display( background );

	  //Show all world objects
	  myWorldObjects.showWorldObjects();

	  //Show character's placed objects
	  player.showPlacedObjects();

	  // change the player's hunger and tiredness status
	  player.ChangeStatus();

	  // Show the background
	  apply_surface( 0, 0, background, screen, &camera );

	  // Show the animals on the screen
	  WildBear.showCharacter();

	  // Checks if any of the animals killed the player
	  WildBear.didKillPlayer(&player);

	  //Show the character on the screen
	  player.showCharacter();

	  playerInventory.display( screen);


	  // Display the hunger and tiredness of the player, as well as
	  // the current day
	  DisplayHunger(screen, player);
	  DisplayTiredness(screen, player);
	  DisplayDay(screen, player);

	  //Cap the frame rate
	  if( fps.get_ticks() < 1000 / framesPerSecond )
            {
	      SDL_Delay( ( 1000 / framesPerSecond ) - fps.get_ticks() );
            }
	  if(!player.IsAlive())
	    {
	      if( SDL_Flip( screen ) == -1 )
                {
		  return 1;
                }
	    }
        }
      while( SDL_PollEvent( &event ) )
        {
	  //If the user has Xed out the window
	  if( event.type == SDL_QUIT )
            {
	      //Quit the program
	      quit = true;
            }
        }
      SDL_Delay(2000);
      quit = true;
    }
  Mix_HaltMusic();
  Mix_FreeMusic(gameMusic);
  
  bool endQuit = false;   //for the ending screen, x out of it


  //All this is to generate a maroon background screen with a game over 
  //message and a pic of prof giving some handy advice on what to do better 
  //next time, C++-style; there are three messages (and some prologue for
  //formating purposes) generated; one is randomly selected and blitted to
  //the screen--die lots to see all 3!!

  SDL_Surface * endScreen = load_image("maroon_title.png"); //background
  TTF_Font * endFont = TTF_OpenFont("ilits.ttf", 96);  //big font
  TTF_Font * smallEndFont = TTF_OpenFont("ilits.ttf", 48); //lil' font
  SDL_Surface *endLine1 = TTF_RenderText_Solid(endFont, "You died!", 
					       introFontColor); //the obvious
  SDL_Surface * endLine2 = TTF_RenderText_Solid(smallEndFont,
						"Thanks for playing!", 
						introFontColor); //polite
  SDL_Surface * emrich = load_image("emrich.png", 128, 0, 0); //Prof. Emrich!
  SDL_Surface * profSays = TTF_RenderText_Solid(smallEndFont, 
						"Coach Emrich advises:", 
						introFontColor); 
  SDL_Surface * nextTime = TTF_RenderText_Solid(smallEndFont, "Next time try",
						introFontColor);
  SDL_Surface * profQuip1 = TTF_RenderText_Solid(smallEndFont,
						"commenting code!", introFontColor);
  SDL_Surface * profQuip2 = TTF_RenderText_Solid(smallEndFont,
						 "using the STL!", 
						 introFontColor);
  SDL_Surface * profQuip3 = TTF_RenderText_Solid(smallEndFont,
						 "making a class!",
						 introFontColor);

  SDL_Surface * whichQuip = NULL;
  switch(rand()%3)
    {
    case 0:
      whichQuip = profQuip1;
      break;
    case 1:
      whichQuip = profQuip2;
      break;
    case 2:
      whichQuip = profQuip3;
      break;
    default:
      break;
    }
      
  while(endQuit == false)
    {
      while(SDL_PollEvent(&event))
	{
	  if(event.type == SDL_KEYDOWN)
	    {
	      if(event.key.keysym.sym == SDLK_ESCAPE)
		{
		  endQuit = true;
		}
	    }
	  else if(event.type == SDL_QUIT)
	    {
	      endQuit = true;
	    }
	}
	
	apply_surface(0, 0, endScreen, screen);
	apply_surface((screenWidth-endLine1->w)/2, 0, endLine1, 
		      screen);
        apply_surface(0, (endLine1->h+profSays->h), profSays, screen);
        apply_surface(0, (endLine1->h+profSays->h+2*(nextTime->h)), nextTime,
		      screen);
        apply_surface(0, (endLine1->h+profSays->h+2*(nextTime->h)+whichQuip->h),
                      whichQuip, screen);
        apply_surface((screenWidth-emrich->w), (screenHeight-emrich->h), emrich,
                      screen);
	apply_surface(0, (screenHeight-endLine2->h),
		      endLine2, screen);
        SDL_Flip(screen);
    }  

  //free the surfaces created for the game over screen
  SDL_FreeSurface(endScreen);
  SDL_FreeSurface(profSays);
  SDL_FreeSurface(nextTime);
  SDL_FreeSurface(profQuip1);
  SDL_FreeSurface(profQuip2);
  SDL_FreeSurface(profQuip3);
  SDL_FreeSurface(endLine1);
  SDL_FreeSurface(endLine2);
  TTF_CloseFont(endFont);
  TTF_CloseFont(smallEndFont);
  
  //Clean up
  clean_up();

  return 0;
}
