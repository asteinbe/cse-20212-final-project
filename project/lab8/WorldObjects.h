#include "MySurface.h"
#include "Entities.h"
#include "Destructibles.h"
#include "Map.h"
#include "Tile.h"
#include <iostream>
#include <vector>
#include <unistd.h>

extern SDL_Surface* background;
extern SDL_Surface* screen;
extern SDL_Surface* berry;
extern SDL_Surface* tree;
extern SDL_Surface* stump;
extern SDL_Surface* rock;
extern SDL_Surface* rubble;
extern void apply_surface( int x, int y, SDL_Surface* source, SDL_Surface* destination, SDL_Rect* clip);
extern bool check_collision( SDL_Rect A, SDL_Rect B );

#ifndef WORLDOBJECTS_H
#define WORLDOBJECTS_H

class WorldObjects
{
    public:
	WorldObjects(Map);
	void generateObjects();
	void removePickup(int,int);
	std::vector<Entities> getPickupsVec();
	std::vector<Destructibles> getBreakablesVec();
	void showWorldObjects();
	void SetDestructibleHealth(int, int, int);	// sets the health of a destructible
	void generate_more_pickups();			// generates more food objects
							// on the map at a constant rate
    private:
	Map theMap;
	std::vector<Entities> pickups;
	std::vector<Destructibles> breakables;
};

WorldObjects::WorldObjects( Map myMap )
{
    theMap = myMap;
    Entities berries( berry, 20, 20 );
    pickups.push_back( berries );
    Destructibles trees( tree, stump, 69, 106, 30, 30 );
    breakables.push_back( trees );
    Destructibles rocks( rock, rubble, 40, 33, 30, 30 );
    breakables.push_back( rocks );
}

void WorldObjects::generateObjects()
{
    srand(SDL_GetTicks());
    int tileCount = (theMap.get_width())*(theMap.get_height());
    int i;
    int currentType;
    int currentX;
    int currentY;
    int addX;
    int addY;
    int chance;
    SDL_Rect tileDims;
    for(i=0;i<tileCount;i++)
    {
        currentType = theMap.get_tile_type( i );
	tileDims = theMap.get_tile_rect( i );
	currentX = tileDims.x;
	currentY = tileDims.y;
	chance = rand() % 100;

	// This if statement adds food to the map
        if(currentType == 2 && chance > 97)
        {
            addX = currentX + (rand() % (tileDims.w - pickups[0].getEntityWidth())); //generate random # from 0 to the tile width minus entity width and add to current X
            addY = currentY + (rand() % (tileDims.h - pickups[0].getEntityHeight()));
	    pickups[0].addEntity( addX, addY );
        }
	else
	{
	    chance = rand() % 100;
	    // this if statement adds trees to the map
	    if(currentType == 2 && chance > 90 && theMap.is_coast( i ) == false) //dont generate on coasts
	    {
                addX = currentX - 20 + (rand() % (tileDims.w - breakables[0].getBeforeWidth())); //mystery numbers are offsets to make trees appear on correct tiles
                addY = currentY - 86 + (rand() % (tileDims.h - breakables[0].getBeforeHeight()));
	        breakables[0].addDestructible( addX, addY );
	    }
	    else if(currentType == 3 && chance > 90 && theMap.is_coast( i ) == false) //dont generate on coasts
	    {
                addX = currentX + (rand() % 20); 
                addY = currentY + (rand() % 20);
	        breakables[1].addDestructible( addX, addY );
	    }
	    
	}
    }
}

void WorldObjects::removePickup( int type, int index ) //first variable is the entity's position in pickups and the second is the specific entity number
{
    pickups[type].removeEntity(index);
}

std::vector<Entities> WorldObjects::getPickupsVec()
{
    return pickups;
}

std::vector<Destructibles> WorldObjects::getBreakablesVec()
{
    return breakables;
}

void WorldObjects::showWorldObjects()
{
    int i;
    for(i=0;i<pickups.size();i++)
    {
        pickups[i].showEntities();
    }
    for(i=0;i<breakables.size();i++)
    {
        breakables[i].showDestructibles();
    }
}

// sets the value of the destructible of type "type" at index "index" to the value "value"
void WorldObjects::SetDestructibleHealth(int type, int index, int value)
{
    breakables[type].SetHealth(index, value);
}

// generates more pickups (food objects) on the map after a certain amount of
// time at random locations.  This also makes sure the locations for placement
// are valid.
void WorldObjects::generate_more_pickups()
{
    srand(SDL_GetTicks());
    int tileCount = (theMap.get_width())*(theMap.get_height());
    int i;
    int tryTile;
    int tryX;
    int tryY;
    int tryType;
    int addX;
    int addY;
    SDL_Rect tileDims;
    bool placed = false;
    for(i=0;i<pickups.size();i++)
    {
        placed = false;
        while(!placed)
        {
            tryTile = rand() % tileCount;		//pick a random tile, if the tile is the right type and location then place a pickup
            tryType = theMap.get_tile_type( tryTile );
            tileDims = theMap.get_tile_rect( tryTile );
            tryX = tileDims.x;
            tryY = tileDims.y;
            if(tryType == i + 2 && theMap.is_coast( tryTile ) == false)
            {
                addX = tryX + (rand() % (tileDims.w - pickups[i].getEntityWidth()));
                addY = tryY + (rand() % (tileDims.h - pickups[i].getEntityHeight()));
                pickups[i].addEntity( addX, addY );
                placed = true;
            }
        }
    }
}
#endif
