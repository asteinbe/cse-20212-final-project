//Andrew Steinbergs
//Lab 7

//The headers
#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include <string>

//Screen attributes
const int screenWidth = 640;
const int screenHeight = 480;
const int screenBPP = 32;

const int levelWidth = 3264;
const int levelHeight = 2448;

//The frames per second
const int framesPerSecond = 10;

//The dimensions of the character
const int redWidth = 64;
const int redHeight = 64;

//The direction status of the character
const int redDown = 0;
const int redUp = 1;
const int redLeft = 2;
const int redRight = 3;

SDL_Rect block;
SDL_Rect camera = { 0, 0, screenWidth, screenHeight };

//The surfaces
SDL_Surface *red = NULL;
SDL_Surface *screen = NULL;
SDL_Surface *background = NULL;

//The event structure
SDL_Event event;

//The areas of the sprite sheet
SDL_Rect clipsUp[ 3 ];
SDL_Rect clipsDown[ 3 ];
SDL_Rect clipsRight[ 2 ];
SDL_Rect clipsLeft[ 2 ];

//The character
class Red
{
    public:
        //Initializes the variables
        Red();
        //Handles input
        void handle_events();
        //Moves the character
        void move();
        void set_camera();
        //Shows the character
        void show();
    private:
        //Character location
        int x;
        int y;
        //Character's rate of movement
        int xVelocity;
        int yVelocity;
        int frame;
        int status;
        //Collision detection block
        SDL_Rect collide_block;
};

//The timer
class Timer
{
     public:
        //Initializes variables
        Timer();
        void start();
        void stop();
        //Gets the timer's time
        int get_ticks();
    private:
        //The clock time when the timer started
        int startTicks;
        //The timer status
        bool started;
};

SDL_Surface *load_image( std::string filename )
{
    //The image that's loaded
    SDL_Surface* loadedImage = NULL;

    //The optimized surface that will be used
    SDL_Surface* optimizedImage = NULL;

    //Load the image
    loadedImage = IMG_Load( filename.c_str() );

    //If the image loaded
    if( loadedImage != NULL )
    {
        //Create an optimized surface
        optimizedImage = SDL_DisplayFormat( loadedImage );

        //Free the old surface
        SDL_FreeSurface( loadedImage );

        //If the surface was optimized
        if( optimizedImage != NULL )
        {
            //Color key surface
            SDL_SetColorKey( optimizedImage, SDL_SRCCOLORKEY, SDL_MapRGB( optimizedImage->format, 0, 0xFF, 0xFF ) );
        }
    }

    //Return the optimized surface
    return optimizedImage;
}

void apply_surface( int x, int y, SDL_Surface* source, SDL_Surface* destination, SDL_Rect* clip = NULL )
{
    //Holds offsets
    SDL_Rect offset;

    //Get offsets
    offset.x = x;
    offset.y = y;

    //Blit
    SDL_BlitSurface( source, clip, destination, &offset );
}

void set_clips()
{
    //Clip the sprites
    clipsDown[ 0 ].x = redWidth * 6;
    clipsDown[ 0 ].y = 0;
    clipsDown[ 0 ].w = redWidth;
    clipsDown[ 0 ].h = redHeight;

    clipsDown[ 1 ].x = 0;
    clipsDown[ 1 ].y = 0;
    clipsDown[ 1 ].w = redWidth;
    clipsDown[ 1 ].h = redHeight;

    clipsDown[ 2 ].x = redWidth;
    clipsDown[ 2 ].y = 0;
    clipsDown[ 2 ].w = redWidth;
    clipsDown[ 2 ].h = redHeight;

    clipsRight[ 0 ].x = redWidth * 2;
    clipsRight[ 0 ].y = 0;
    clipsRight[ 0 ].w = redWidth;
    clipsRight[ 0 ].h = redHeight;

    clipsRight[ 1 ].x = redWidth * 3;
    clipsRight[ 1 ].y = 0;
    clipsRight[ 1 ].w = redWidth;
    clipsRight[ 1 ].h = redHeight;

    clipsLeft[ 0 ].x = redWidth * 4;
    clipsLeft[ 0 ].y = 0;
    clipsLeft[ 0 ].w = redWidth;
    clipsLeft[ 0 ].h = redHeight;

    clipsLeft[ 1 ].x = redWidth * 5;
    clipsLeft[ 1 ].y = 0;
    clipsLeft[ 1 ].w = redWidth;
    clipsLeft[ 1 ].h = redHeight;

    clipsUp[ 0 ].x = redWidth * 9;
    clipsUp[ 0 ].y = 0;
    clipsUp[ 0 ].w = redWidth;
    clipsUp[ 0 ].h = redHeight;

    clipsUp[ 1 ].x = redWidth * 10;
    clipsUp[ 1 ].y = 0;
    clipsUp[ 1 ].w = redWidth;
    clipsUp[ 1 ].h = redHeight;

    clipsUp[ 2 ].x = redWidth * 11;
    clipsUp[ 2 ].y = 0;
    clipsUp[ 2 ].w = redWidth;
    clipsUp[ 2 ].h = redHeight;
}

bool init()
{
    //Initialize all SDL subsystems
    if( SDL_Init( SDL_INIT_EVERYTHING ) == -1 )
    {
        return false;
    }

    //Set up the screen
    screen = SDL_SetVideoMode( screenWidth, screenHeight, screenBPP, SDL_SWSURFACE );

    //If there was an error in setting up the screen
    if( screen == NULL )
    {
        return false;
    }

    //Set the window caption
    SDL_WM_SetCaption( "Character Collision Test", NULL );

    //If everything initialized fine
    return true;
}

bool load_files()
{
    //Load the sprite sheet
    red = load_image( "red.png" );

    background = load_image ( "grass.jpg" );

    //If there was a problem in loading the sprite
    if( red == NULL )
    {
        return false;
    }

    if( background == NULL )
    {
        return false;
    }

    //If everything loaded fine
    return true;
}

void clean_up()
{
    //Free all surfaces
    SDL_FreeSurface( red );
    SDL_FreeSurface( background );

    //Quit SDL
    SDL_Quit();
}

bool check_collision( SDL_Rect A, SDL_Rect B )
{
    //The sides of the rectangles
    int leftA, leftB;
    int rightA, rightB;
    int topA, topB;
    int bottomA, bottomB;

    //Calculate the sides of rect A
    leftA = A.x;
    rightA = A.x + A.w;
    topA = A.y;
    bottomA = A.y + A.h;

    //Calculate the sides of rect B
    leftB = B.x;
    rightB = B.x + B.w;
    topB = B.y;
    bottomB = B.y + B.h;

    //If any of the sides from A are outside of B
    if( bottomA <= topB )
    {
        return false;
    }

    if( topA >= bottomB )
    {
        return false;
    }

    if( rightA <= leftB )
    {
        return false;
    }

    if( leftA >= rightB )
    {
        return false;
    }

    //If none of the sides from A are outside B
    return true;
}

Red::Red()
{
    //Initialize movement variables
    x = 0;
    y = 0;
    xVelocity = 0;
    yVelocity = 0;
    collide_block.h = redHeight / 2;
    collide_block.w = redWidth;
    collide_block.x = x;
    collide_block.y = y + collide_block.h;

    //Initialize animation variables
    frame = 0;
    status = redDown;
}

void Red::handle_events()
{
    //If a key was pressed
    if( event.type == SDL_KEYDOWN )
    {
        //Set the velocity
        switch( event.key.keysym.sym )
        {
            case SDLK_RIGHT: xVelocity += redWidth / 4; break;
            case SDLK_LEFT: xVelocity -= redWidth / 4; break;
            case SDLK_UP: yVelocity -= redHeight / 4; break;
            case SDLK_DOWN: yVelocity += redHeight / 4; break;
        }
    }
    //If a key was released
    else if( event.type == SDL_KEYUP )
    {
        //Set the velocity
        switch( event.key.keysym.sym )
        {
            case SDLK_RIGHT: xVelocity -= redWidth / 4; break;
            case SDLK_LEFT: xVelocity += redWidth / 4; break;
            case SDLK_UP: yVelocity += redHeight / 4; break;
            case SDLK_DOWN: yVelocity -= redHeight / 4; break;
        }
    }
}

void Red::move()
{
    //Move the character left or right, and reset the collision block to the new location
    x += xVelocity;
    collide_block.x = x;
    
    //If it went too far to the left or right
    if( ( x < 0 ) || ( x + redWidth > levelWidth ) || ( check_collision( block, collide_block ) ) )
    {
        //move back
        x -= xVelocity;   
    }

    //Move the character up or down, and reset the collision block to the new location
    y += yVelocity;
    collide_block.y = y + collide_block.h;
    
    //If it went too far to the left or right
    if( ( y < 0 ) || ( y + redHeight > levelHeight ) || ( check_collision( block, collide_block ) ) )
    {
        //move back
        y -= yVelocity;    
    }

    collide_block.x = x;
    collide_block.y = y + collide_block.h;

}

void Red::show()
{
    if( yVelocity < 0 )
    {
        //Set the animation to left
        status = redUp;

        //Move to the next frame in the animation
        frame++;
    }
    else if( yVelocity > 0 )
    {
        //Set the animation to right
        status = redDown;

        //Move to the next frame in the animation
        frame++;
    }
    else if( xVelocity < 0 )
    {
        //Set the animation to right
        status = redLeft;

        //Move to the next frame in the animation
        frame++;
    }
    else if( xVelocity > 0 )
    {
        //Set the animation to right
        status = redRight;

        //Move to the next frame in the animation
        frame++;
    }
    //If Red is standing
    else
    {
        //Restart the animation
        frame = 0;
    }

    //Loop the animation
    if( status == redRight || status == redLeft )
    {
        if ( frame >= 2 )
        {
            frame = 0;
        }
    }
    else if( status == redUp || status == redDown )
    {
        if ( frame >= 3 )
        {
            frame = 0;
        }
    }

    //Show the character
    if( status == redDown )
    {
        apply_surface( x - camera.x, y - camera.y, red, screen, &clipsDown[ frame ] );
    }
    else if( status == redUp )
    {
        apply_surface( x - camera.x, y - camera.y, red, screen, &clipsUp[ frame ] );
    }
    else if( status == redLeft )
    {
        apply_surface( x - camera.x, y - camera.y, red, screen, &clipsLeft[ frame ] );
    }
    else if( status == redRight )
    {
        apply_surface( x - camera.x, y - camera.y, red, screen, &clipsRight[ frame ] );
    }
}

void Red::set_camera()
{
    //Center the camera over the dot
    camera.x = ( x + redWidth / 2 ) - screenWidth / 2;
    camera.y = ( y + redHeight / 2 ) - screenHeight / 2;

    //Keep the camera in bounds.
    if( camera.x < 0 )
    {
        camera.x = 0;
    }
    if( camera.y < 0 )
    {
        camera.y = 0;
    }
    if( camera.x > levelWidth - camera.w )
    {
        camera.x = levelWidth - camera.w;
    }
    if( camera.y > levelHeight - camera.h )
    {
        camera.y = levelHeight - camera.h;
    }
}

Timer::Timer()
{
    //Initialize the variables
    startTicks = 0;
    started = false;
}

void Timer::start()
{
    //Start the timer
    started = true;

    //Get the current clock time
    startTicks = SDL_GetTicks();
}

void Timer::stop()
{
    //Stop the timer
    started = false;
}

int Timer::get_ticks()
{
    //If the timer is running
    if( started == true )
    {
        //Return the current time minus the start time
        return SDL_GetTicks() - startTicks;
    }

    //If the timer isn't running
    return 0;
}

int main( int argc, char* args[] )
{
    //Quit flag
    bool quit = false;

    //Initialize
    if( init() == false )
    {
        return 1;
    }

    //Load the files
    if( load_files() == false )
    {
        return 1;
    }

    //Clip the sprite sheet
    set_clips();

    //The frame rate regulator
    Timer fps;

    //The character
    Red player;

    block.x = 400;
    block.y = 400;
    block.w = 40;
    block.h = 20;

    //While the user hasn't quit
    while( quit == false )
    {
        //Start the frame timer
        fps.start();

        //While there's events to handle
        while( SDL_PollEvent( &event ) )
        {
            //Handle events for the stick figure
            player.handle_events();

            //If the user has Xed out the window
            if( event.type == SDL_QUIT )
            {
                //Quit the program
                quit = true;
            }
        }

        //Move Red
        player.move();

	player.set_camera();

        //Apply background
        apply_surface( 0, 0, background, screen, &camera ); 

        SDL_FillRect( background, &block, SDL_MapRGB( screen->format, 0xFF, 0, 0 ) );

        //Show the character on the screen
        player.show();

        //Update the screen
        if( SDL_Flip( screen ) == -1 )
        {
            return 1;
        }

        //Cap the frame rate
        if( fps.get_ticks() < 1000 / framesPerSecond )
        {
            SDL_Delay( ( 1000 / framesPerSecond ) - fps.get_ticks() );
        }
    }

    //Clean up
    clean_up();

    return 0;
}
